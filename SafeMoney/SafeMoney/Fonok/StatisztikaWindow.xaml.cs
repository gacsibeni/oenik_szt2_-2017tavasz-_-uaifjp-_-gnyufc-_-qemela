﻿// <copyright file="StatisztikaWindow.xaml.cs" company="BBK Software">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SafeMoney.Fonok
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;

    /// <summary>
    /// Interaction logic for StatisztikaWindow.xaml
    /// </summary>
    public partial class StatisztikaWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StatisztikaWindow"/> class.
        /// </summary>
        public StatisztikaWindow()
        {
            this.InitializeComponent();
        }
    }
}
