﻿// <copyright file="UgyvezetoKezeloWindow.xaml.cs" company="BBK Software">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SafeMoney.Fonok
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using DataAccessLayer;

    /// <summary>
    /// Interaction logic for UgyfelKezeloWindow.xaml
    /// </summary>
    public partial class UgyintezoKezeloWindow : Window
    {
        private UGYINTEZO ugyint;

        /// <summary>
        /// Initializes a new instance of the <see cref="UgyintezoKezeloWindow"/> class.
        /// </summary>
        /// <param name="ugyintezo">Ugyintezo referencia</param>
        public UgyintezoKezeloWindow(UGYINTEZO ugyintezo)
        {
            this.InitializeComponent();
            if (ugyintezo != null)
            {
                this.ugyint = ugyintezo;
            }
            else
            {
                this.ugyint = new UGYINTEZO();
            }

            this.DataContext = ugyintezo;
        }

        /// <summary>
        /// Gets ugyintezo
        /// </summary>
        public UGYINTEZO Ugyint
        {
            get
            {
                return this.ugyint;
            }
        }

        /// <summary>
        /// Ügyfezető mentése
        /// </summary>
        /// <param name="sender">Nem használt.</param>
        /// <param name="e">Nem használt</param>
        private void Mentes(object sender, RoutedEventArgs e)
        {
            if (this.pw1.Password == this.pw2.Password)
            {
                this.ugyint.NEV = this.textBox_Nev.Text;
                this.ugyint.USERNAME = this.textBox_Felhasznalonev.Text;
                if (this.pw1.Password != string.Empty)
                {
                    this.ugyint.PW = this.pw1.Password;
                }

                this.DialogResult = true;
            }
            else
            {
                MessageBox.Show("A két jelszó nem egyezik meg!");
            }
        }

        /// <summary>
        /// Kilépés mentés nélkül
        /// </summary>
        /// <param name="sender">Nem használt</param>
        /// <param name="e">Nem használt.</param>
        private void Kilepes(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
