﻿// <copyright file="Adozas.xaml.cs" company="BBK Software">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SafeMoney.Fonok
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using BusinessLogic;
    using BusinessLogic.FonokLekerdezesek;

    /// <summary>
    /// Interaction logic for Adozas.xaml
    /// </summary>
    public partial class AdozasWindow : Window
    {
        private FonokViewModel fvm;

        /// <summary>
        /// Initializes a new instance of the <see cref="AdozasWindow"/> class.
        /// Létrehozza az objektumot.
        /// </summary>
        /// <param name="fvm">View model átadása</param>
        public AdozasWindow(FonokViewModel fvm)
        {
            this.InitializeComponent();
            this.fvm = fvm;
        }

        /// <summary>
        /// Kimutatást generál.
        /// </summary>
        /// <param name="sender">Nem használt</param>
        /// <param name="e">Nem használt.</param>
        private void Generalas(object sender, RoutedEventArgs e)
        {
            AdozasMetodusok.KimutatasGeneralas((DateTime)this.DateTime_indulo.SelectedDate, (DateTime)this.DateTime_vegso.SelectedDate, this.fvm);
        }

        /// <summary>
        /// Ablak bezárása.
        /// </summary>
        /// <param name="sender">Nem használt</param>
        /// <param name="e">Nem használt.</param>
        private void Kilepes(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
