﻿// <copyright file="Diagram.xaml.cs" company="BBK Software">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SafeMoney.Fonok
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using BusinessLogic;
    using BusinessLogic.FonokLekerdezesek;
    using Microsoft.Research.DynamicDataDisplay;
    using Microsoft.Research.DynamicDataDisplay.DataSources;

    /// <summary>
    /// Interaction logic for Diagram.xaml
    /// </summary>
    public partial class Diagram : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Diagram"/> class.
        /// </summary>
        /// <param name="fvm">FonokViewModel referencia</param>
        public Diagram(FonokViewModel fvm)
        {
            this.InitializeComponent();
            List<DateTime> datumok = new List<DateTime>();
            DiagramMetodusok.DatumokFeltolt(datumok, fvm);
            var datesDataSource = new EnumerableDataSource<DateTime>(datumok);
            datesDataSource.SetXMapping(x => this.dateAxis.ConvertToDouble(x));
            DiagramMetodusok m = new DiagramMetodusok(fvm, datumok);
            datesDataSource.SetYMapping(y =>
            {
                double vissza = m.DatumParosit(y);
                return vissza;
            });
            this.plotter.AddLineGraph(datesDataSource, 2);
        }
    }
}
