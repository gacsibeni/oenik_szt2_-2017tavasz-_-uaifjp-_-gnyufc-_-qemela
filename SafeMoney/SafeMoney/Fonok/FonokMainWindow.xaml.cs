﻿// <copyright file="FonokMainWindow.xaml.cs" company="BBK Software">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SafeMoney
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using BusinessLogic;
    using DataAccessLayer;
    using SafeMoney.Fonok;

    /// <summary>
    /// Interaction logic for FonokMainWindow.xaml
    /// </summary>
    public partial class FonokMainWindow : Window
    {
        private FonokViewModel fVM;

        /// <summary>
        /// Initializes a new instance of the <see cref="FonokMainWindow"/> class.
        /// </summary>
        public FonokMainWindow()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Ablak betöltése utáni folyamatok.
        /// </summary>
        /// <param name="sender">Nem használt</param>
        /// <param name="e">Nem használt.</param>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.fVM = new FonokViewModel();
            this.DataContext = this.fVM;
        }

        /// <summary>
        /// Statisztika megjelenítése.
        /// </summary>
        /// <param name="sender">Nem használt</param>
        /// <param name="e">Nem használt.</param>
        private void Statisztika(object sender, RoutedEventArgs e)
        {
            Diagram d = new Diagram(this.fVM);
            d.ShowDialog();
        }

        /// <summary>
        /// Új ügyintéző létrehozása.
        /// </summary>
        /// <param name="sender">Nem használt</param>
        /// <param name="e">Nem használt.</param>
        private void Letrehozas(object sender, RoutedEventArgs e)
        {
                UgyintezoKezeloWindow kezel = new UgyintezoKezeloWindow(null);
                if (kezel.ShowDialog() == true)
                {
                    this.fVM.UgyintezoRepository.Insert(kezel.Ugyint);
                    this.fVM.Ugyintezok.Add(kezel.Ugyint);
                }
        }

        /// <summary>
        /// Ügyintézői fiók kezelése.
        /// </summary>
        /// <param name="sender">Nem használt</param>
        /// <param name="e">Nem használt.</param>
        private void Kezeles(object sender, RoutedEventArgs e)
        {
            if (this.listBox.SelectedItem != null)
            {
                UgyintezoKezeloWindow kezel = new UgyintezoKezeloWindow((UGYINTEZO)this.listBox.SelectedItem);
                if (kezel.ShowDialog() == true)
                {
                    // FVM.UgyintezoRepository.ChangeName() Db-be változások beállítása!!!
                }
            }
            else
            {
                MessageBox.Show("Kérem válassza ki a kezelni kívánt ügyintézőt!");
            }
        }

        /// <summary>
        /// Ügyintézői fiók törlése.
        /// </summary>
        /// <param name="sender">Nem használt</param>
        /// <param name="e">Nem használt.</param>
        private void Törles(object sender, RoutedEventArgs e)
        {
            if (this.listBox.SelectedItem != null)
            {
                this.fVM.UgyintezoRepository.Delete((UGYINTEZO)this.listBox.SelectedItem);
                this.fVM.Ugyintezok.Remove((UGYINTEZO)this.listBox.SelectedItem);
            }
        }

        /// <summary>
        /// Adózás
        /// </summary>
        /// <param name="sender">Nem használt</param>
        /// <param name="e">Nem használt.</param>
        private void Adozas(object sender, RoutedEventArgs e)
        {
            AdozasWindow aw = new AdozasWindow(this.fVM);
            aw.ShowDialog();
        }

        /// <summary>
        /// Ablak bezárása.
        /// </summary>
        /// <param name="sender">Nem használt</param>
        /// <param name="e">Nem használt.</param>
        private void Kilepes(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
