﻿// <copyright file="SzamlakezeloWindow.xaml.cs" company="BBK Software">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SafeMoney.Ugyintezo
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using BusinessLogic;

    /// <summary>
    /// Interaction logic for SzamlakezeloWindow.xaml
    /// </summary>
    public partial class SzamlakezeloWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SzamlakezeloWindow"/> class.
        /// </summary>
        public SzamlakezeloWindow()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Számla mentése.
        /// </summary>
        /// <param name="sender">Nem használt</param>
        /// <param name="e">Nem használt.</param>
        private void Mentes(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            this.Close();
        }

        /// <summary>
        /// Ablak betöltése utáni feladatok elvégzése.
        /// </summary>
        /// <param name="sender">Nem használt</param>
        /// <param name="e">Nem használt.</param>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.szamlaszamtxt.IsEnabled = false;
        }

        /// <summary>
        /// Változások eldobása, ablakból való kilépés.
        /// </summary>
        /// <param name="sender">Nem használt</param>
        /// <param name="e">Nem használt.</param>
        private void Megse(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }
    }
}
