﻿// <copyright file="UgyintezoMainWindow.xaml.cs" company="BBK Software">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SafeMoney
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using BusinessLogic;
    using BusinessLogic.Kivetelek;
    using DataAccessLayer;
    using SafeMoney.Ugyintezo;

    /// <summary>
    /// Interaction logic for UgyintezoMainWindow.xaml
    /// </summary>
    public partial class UgyintezoMainWindow : Window
    {
        private UgyintezoViewModel uIV;

        /// <summary>
        /// Initializes a new instance of the <see cref="UgyintezoMainWindow"/> class.
        /// </summary>
        public UgyintezoMainWindow()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Ablak betöltése utáni feladatok elvégzése.
        /// </summary>
        /// <param name="sender">Nem használt.</param>
        /// <param name="e">Nem használt</param>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.uIV = new UgyintezoViewModel();
            this.DataContext = this.uIV;
        }

        /// <summary>
        /// Kiválasztott számla módosítása
        /// </summary>
        /// <param name="sender">Nem használt.</param>
        /// <param name="e">Nem használt</param>
        private void SzamlaModositas(object sender, RoutedEventArgs e)
        {
            if (this.szamlaLB.SelectedItem != null)
            {
                SZAMLA s = (SZAMLA)this.szamlaLB.SelectedItem;
                this.uIV.AktivSzamla = new SZAMLA();
                this.uIV.SzamlaMasolas(s, this.uIV.AktivSzamla);
                SzamlakezeloWindow sW = new SzamlakezeloWindow();
                sW.DataContext = this.uIV;
                if (sW.ShowDialog() == true)
                {
                    this.uIV.SzamlaModositas(s);

                    // UIV.SzamlaRepository.ChangeSzamlaData(s);
                }
            }
            else
            {
                MessageBox.Show("Ki kell választaia egy számlát!");
            }
        }

        /// <summary>
        /// Új számla létrehozása.
        /// </summary>
        /// <param name="sender">Nem használt.</param>
        /// <param name="e">Nem használt</param>
        private void UjSzamla(object sender, RoutedEventArgs e)
        {
            this.uIV.AktivSzamla = new SZAMLA();
            this.uIV.AktivSzamla.SZAMLASZAM = this.uIV.KovetkezoSzamlaID();
            SzamlakezeloWindow sW = new SzamlakezeloWindow();
            sW.DataContext = this.uIV;
            if (sW.ShowDialog() == true)
            {
                this.uIV.AktivSzamla.STARTDATE = DateTime.Now;
                this.uIV.UjSzamla();
            }
        }

        /// <summary>
        /// Számlát keres az adatbázisban.
        /// </summary>
        /// <param name="sender">Nem használt</param>
        /// <param name="e">Nem használt.</param>
        private void SzamlaKereses(object sender, RoutedEventArgs e)
        {
            try
            {
                decimal s = decimal.Parse(this.txtSzamlaKeres.Text);
                if (s != 0)
                {
                    if (this.uIV.Szamlak.Contains(this.uIV.SzamlaKereses(s)))
                    {
                        this.szamlaLB.SelectedItem = this.uIV.SzamlaKereses(s);
                        this.SzamlaModositas(null, null);
                    }
                    else
                    {
                        MessageBox.Show("A számlaszám nem létezik.");
                    }
                }
            }
            catch (FormatException r)
            {
                MessageBox.Show("A keresendő számlaszám formátuma nem megfelelő. " + r.Message);
            }
        }

        /// <summary>
        /// Ügyfél kezelő.
        /// </summary>
        /// <param name="sender">Nem használt.</param>
        /// <param name="e">Nem használt</param>
        private void UgyfelKezeles(object sender, RoutedEventArgs e)
        {
            if (this.ugyfelekLB.SelectedItem != null)
            {
                this.uIV.AktivUgyfel = new UGYFEL();
                this.uIV.UgyfelMasolas((UGYFEL)this.ugyfelekLB.SelectedItem, this.uIV.AktivUgyfel);
                UgyfelkezeloWindow uK = new UgyfelkezeloWindow();
                uK.DataContext = this.DataContext;
                if (uK.ShowDialog() == true)
                {
                    this.uIV.UgyfelNevModositasa();
                    try
                    {
                        this.uIV.UgyfelMentesTorlesEllenorzes();
                    }
                    catch (UgyfelSzamlaFuggosegException s)
                    {
                        MessageBox.Show(s.Message);
                    }

                    this.uIV.UgyfelModositas((UGYFEL)this.ugyfelekLB.SelectedItem);
                }
            }
            else
            {
                MessageBox.Show("Ki kell választania egy ügyfelet.");
            }
        }

        /// <summary>
        /// Új ügyfél létrehozása
        /// </summary>
        /// <param name="sender">Nem használt</param>
        /// <param name="e">Nem használt.</param>
        private void UjUgyfel(object sender, RoutedEventArgs e)
        {
            this.uIV.AktivUgyfel = new UGYFEL();
            UgyfelkezeloWindow uK = new UgyfelkezeloWindow();
            uK.DataContext = this.DataContext;
            if (uK.ShowDialog() == true)
            {
                this.uIV.UgyfelNevModositasa();
                this.uIV.UjUgyfel();
            }
        }

        /// <summary>
        /// Ügyfél törlés click eseményt kezel
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">e</param>
        private void UgyefelTorles(object sender, RoutedEventArgs e)
        {
            if (this.ugyfelekLB.SelectedItem != null)
            {
                try
                {
                    this.uIV.AktivUgyfel = (UGYFEL)this.ugyfelekLB.SelectedItem;
                    this.uIV.UgyfelMentesTorlesEllenorzes();
                    this.uIV.Ugyfelek.Remove(this.uIV.AktivUgyfel);
                }
                catch (UgyfelSzamlaFuggosegException s)
                {
                    MessageBox.Show(s.Message);
                }
            }
            else
            {
                MessageBox.Show("Ki kell választania egy ügyfelet.");
            }
        }

        /// <summary>
        /// Törli a kiválasztott számlát.
        /// </summary>
        /// <param name="sender">Nem használt</param>
        /// <param name="e">Nem használt.</param>
        private void SzamlaTörles(object sender, RoutedEventArgs e)
        {
            this.uIV.AktivSzamla = (SZAMLA)this.szamlaLB.SelectedItem;
            this.uIV.SzamlaTorles();
        }

        /// <summary>
        /// Ügyfél keres click kezelés
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private void UgyfelKeres(object sender, RoutedEventArgs e)
        {
            this.uIV.UgyfelKeres(this.ugyfelKeresTXT.Text);
        }

        /// <summary>
        /// Visszaállítja a keresőt alapállapotba (Tehát nincsen szűkítés)
        /// </summary>
        /// <param name="sender">Nem használt.</param>
        /// <param name="e">Nem használt</param>
        private void UgyfelkeresAlap(object sender, RoutedEventArgs e)
        {
            this.uIV.UgyfelKeres(string.Empty);
            this.ugyfelKeresTXT.Text = "Ügyfél neve";
        }

        /// <summary>
        /// Tranzakció adatainak megjelenítése
        /// </summary>
        /// <param name="sender">Nem használt.</param>
        /// <param name="e">Nem használt</param>
        private void TranzakcioMegtekintes(object sender, RoutedEventArgs e)
        {
            if (this.tranzakcioLB.SelectedItem != null)
            {
                TranzakcioMegjelenitoWindow tMW = new TranzakcioMegjelenitoWindow();
                tMW.DataContext = (TRANZAKCIO)this.tranzakcioLB.SelectedItem;
                tMW.ShowDialog();
            }
            else
            {
                MessageBox.Show("Válasszon ki egy tranzakciót.");
            }
        }
    }
}
