﻿// <copyright file="UgyfelkezeloWindow.xaml.cs" company="BBK Software">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SafeMoney.Ugyintezo
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using BusinessLogic;
    using BusinessLogic.Kivetelek;
    using DataAccessLayer;

    /// <summary>
    /// Interaction logic for UgyfelkezeloWindow.xaml
    /// </summary>
    public partial class UgyfelkezeloWindow : Window
    {
        private UgyintezoViewModel uIV;
        private bool passwordKell = false;
        private string uN;

        /// <summary>
        /// Initializes a new instance of the <see cref="UgyfelkezeloWindow"/> class.
        /// </summary>
        public UgyfelkezeloWindow()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Számla kezelése
        /// </summary>
        /// <param name="sender">Nem használt</param>
        /// <param name="e">Nem használt.</param>
        private void SzamlaKezeles(object sender, RoutedEventArgs e)
        {
            if (this.szamlaCB.SelectedItem != null)
            {
                SZAMLA s = (SZAMLA)this.szamlaCB.SelectedItem;
                this.uIV.AktivSzamla = new SZAMLA();
                this.uIV.SzamlaMasolas(s, this.uIV.AktivSzamla);
                SzamlakezeloWindow sW = new SzamlakezeloWindow();
                sW.DataContext = this.uIV;
                if (sW.ShowDialog() == true)
                {
                    this.uIV.SzamlaMasolas(this.uIV.AktivSzamla, s);
                }
            }
        }

        /// <summary>
        /// Ablak betöltődés után első tevékenységek.
        /// </summary>
        /// <param name="sender">Nem használt</param>
        /// <param name="e">Nem használt.</param>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.uIV = (UgyintezoViewModel)this.DataContext;
            this.uN = this.uIV.AktivUgyfel.USERNAME;
            if (this.uIV.AktivUgyfel.ID == 0)
            {
                this.uIV.KovetkezoUgyfelaID();
                this.passwordKell = true;
            }
        }

        /// <summary>
        /// Nem menti el.
        /// </summary>
        /// <param name="sender">Nem használt</param>
        /// <param name="e">Nem használt.</param>
        private void Megse(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        /// <summary>
        /// Módosítások elmentése.
        /// </summary>
        /// <param name="sender">Nem használt</param>
        /// <param name="e">Nem használt.</param>
        private void Mentes(object sender, RoutedEventArgs e)
        {
            if ((this.uN == null || this.uIV.AktivUgyfel.USERNAME.ToUpper() != this.uN.ToUpper()) && !this.uIV.UsernameEllenőrzés())
            {
                MessageBox.Show("A felhasználónév már foglalt.");
            }
            else if (this.passwordKell)
            {
                if (this.pw2.Password == this.pw1.Password && this.pw1.Password != string.Empty)
                {
                    this.uIV.AktivUgyfel.PW = this.pw1.Password;
                    this.DialogResult = true;
                    this.Close();
                }
                else
                {
                    MessageBox.Show("A két jelszó nem egyezik meg. (A jelszó mezők nem lehetnek üresen)");
                }
            }
            else
            {
                if (this.pw1.Password != string.Empty && this.pw2.Password == this.pw1.Password)
                {
                    this.uIV.AktivUgyfel.PW = this.pw1.Password;
                }

                this.DialogResult = true;
                this.Close();
            }
        }
    }
}
