﻿using System;
using System.Runtime.Serialization;

namespace SafeMoney.Ugyintezo
{
    [Serializable]
    internal class UgyfelSzamlaFuggosegException : Exception
    {
        public UgyfelSzamlaFuggosegException()
        {
        }

        public UgyfelSzamlaFuggosegException(string message) : base(message)
        {
        }

        public UgyfelSzamlaFuggosegException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected UgyfelSzamlaFuggosegException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}