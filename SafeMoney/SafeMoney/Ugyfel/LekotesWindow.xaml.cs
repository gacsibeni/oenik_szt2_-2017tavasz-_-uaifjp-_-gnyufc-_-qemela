﻿// <copyright file="LekotesWindow.xaml.cs" company="BBK Software">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SafeMoney.Ugyfel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using BusinessLogic.ViewModelek;
    using DataAccessLayer;

    /// <summary>
    /// Interaction logic for LekotesWindow.xaml
    /// </summary>
    public partial class LekotesWindow : Window
    {
        /// <summary>
        /// ViewModel
        /// </summary>
        private LekotesViewModel lvm;

        /// <summary>
        /// Initializes a new instance of the <see cref="LekotesWindow"/> class.
        /// Lekötés ablakot <see cref="LekotesWindow"/>  létrehozó konstruktor.
        /// </summary>
        /// <param name="ugyfel">Ügyfél aki nyitja az ablakot.</param>
        /// <param name="entities">Szülő ablaktól érkező dbentity.</param>
        public LekotesWindow(UGYFEL ugyfel, DatabaseEntities entities)
        {
            this.InitializeComponent();
            this.lvm = new LekotesViewModel(ugyfel, entities);
            this.DataContext = this.lvm;
        }

        private void TextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            e.Handled = (e.Key < Key.D0 || e.Key > Key.D9) && (e.Key < Key.NumPad0 || e.Key > Key.NumPad9);
        }

        /// <summary>
        /// Lekötés elmentése.
        /// </summary>
        /// <param name="sender">Nem használt</param>
        /// <param name="e">Nem használt.</param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Random r = new Random();
            if (this.SumBox.Text != null)
            {
                try
                {
                    decimal sum = decimal.Parse(this.SumBox.Text);
                    if (sum > 0 && sum < this.lvm.Ugyfel.SZAMLA.EGYENLEG)
                    {
                        DateTime date = DateTime.Now;
                        LEKOTES ujlekotes = new LEKOTES() { LEKOTESSZAM = r.Next(300, 342341), BE = sum, STARTDATE = date, ENDDATE = date.AddYears(1), SZAMLASZAM = this.lvm.Ugyfel.SZAMLASZAM, VISSZA = sum * 1.15M };
                        this.lvm.LekotesRepository.Insert(ujlekotes);
                        this.lvm.VanLekotese = true;
                        this.lvm.Lekotes = ujlekotes;
                    }
                }
                catch (FormatException)
                {
                    MessageBox.Show("Hibás összeg!");
                }
            }
        }
    }
}
