﻿// <copyright file="BefizetesWindow.xaml.cs" company="BBK Software">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SafeMoney.Ugyfel
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using BusinessLogic;
    using DataAccessLayer;

    /// <summary>
    /// Interaction logic for BefizetesWindow.xaml
    /// </summary>
    public partial class BefizetesWindow : Window
    {
        /// <summary>
        /// Bejelentkezett ügyfél.
        /// </summary>
        private UGYFEL ugyfel2;

        /// <summary>
        /// ViewModel
        /// </summary>
        private BefizetesViewModel bvm;

        /// <summary>
        /// Initializes a new instance of the <see cref="BefizetesWindow"/> class.
        /// Létrehoz egy <see cref="BefizetesWindow"/> példányt.
        /// </summary>
        /// <param name="ugyfel">Nyitó ügyfél.</param>
        /// <param name="entities">Szülőablaktól kapott entites.</param>
        public BefizetesWindow(UGYFEL ugyfel, DatabaseEntities entities)
        {
            this.InitializeComponent();
            this.ugyfel2 = ugyfel;
            this.bvm = new BefizetesViewModel(entities);
        }

        /// <summary>
        /// Befizetés
        /// </summary>
        /// <param name="sender">Nem használt</param>
        /// <param name="e">Nem használt.</param>
        private void Befizet(object sender, RoutedEventArgs e)
        {
            try
            {
                int osszeg = int.Parse(this.SumBox.Text);
                this.bvm.SzamlaRepository.ChangeFunds(this.ugyfel2.SZAMLASZAM, true, osszeg);
                MessageBox.Show("Sikeres befizetés!");
                this.DialogResult = true;
            }
            catch (DbUpdateException f)
            {
                MessageBox.Show("HIBA! Tranzakció sikertelen! /n A hiba oka: " + f.Message);
            }
            catch (OverflowException o)
            {
                MessageBox.Show("Túl nagy összeg!" + "(" + o.Message + ")");
            }
        }

        private void SumBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            e.Handled = (e.Key < Key.D0 || e.Key > Key.D9) && (e.Key < Key.NumPad0 || e.Key > Key.NumPad9);
        }
    }
}
