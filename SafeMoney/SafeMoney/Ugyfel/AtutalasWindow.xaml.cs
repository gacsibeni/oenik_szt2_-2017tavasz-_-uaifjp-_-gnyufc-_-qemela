﻿// <copyright file="AtutalasWindow.xaml.cs" company="BBK Software">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SafeMoney.Ugyfel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using BusinessLogic;
    using DataAccessLayer;

    /// <summary>
    /// Interaction logic for AtutalasWindow.xaml
    /// </summary>
    public partial class AtutalasWindow : Window
    {
        private AtutalasUgyfelViewModel avm;

        /// <summary>
        /// Initializes a new instance of the <see cref="AtutalasWindow"/> class.
        /// Létrehoz egy <see cref="AtutalasWindow"/> osztálypéldányt.
        /// </summary>
        /// <param name="ugyfel">Ablakot nyitó ügyfél.</param>
        /// <param name="entities">Kapott entities.</param>
        public AtutalasWindow(UGYFEL ugyfel, DatabaseEntities entities)
        {
            this.InitializeComponent();
            this.avm = new AtutalasUgyfelViewModel(ugyfel, entities);
            this.DataContext = this.avm;
        }

        /// <summary>
        /// Módosítások eldobása.
        /// </summary>
        /// <param name="sender">Nem használt</param>
        /// <param name="e">Nem használt.</param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// A változtatások elmentése
        /// </summary>
        /// <param name="sender">Nem használt.</param>
        /// <param name="e">Nem használt</param>
        private void Mentes(object sender, RoutedEventArgs e)
        {
            SZAMLA cel = this.avm.Kivalasztott;

            try
            {
                int osszeg = int.Parse(this.SumBox.Text);

                if (cel != null)
                {
                    if (cel.SZAMLASZAM != this.avm.Ugyfel.SZAMLASZAM)
                    {
                        Random rnd = new Random();

                        this.avm.SzamlaRepository.ChangeFunds(cel.SZAMLASZAM, true, osszeg);
                        this.avm.SzamlaRepository.ChangeFunds(this.avm.Ugyfel.SZAMLASZAM, false, osszeg);
                        this.avm.TranzakcioRepository.Insert(new TRANZAKCIO { SZAMLASZAM = this.avm.Ugyfel.SZAMLASZAM, CEL = cel.SZAMLASZAM, DATUM = DateTime.Now, OSSZEG = osszeg, SZAMLA = this.avm.Ugyfel.SZAMLA, ID = rnd.Next(1, 500000) });
                        MessageBox.Show("Tranzakció sikeres!");
                        this.DialogResult = true;
                    }
                    else
                    {
                        MessageBox.Show("Önmagának nem utalhat!");
                    }
                }
                else
                {
                    MessageBox.Show("Nem választott ki számlát!");
                }
            }
            catch (OverflowException o)
            {
                MessageBox.Show("Túl nagy összeg!" + "(" + o.Message + ")");
            }
        }

        private void SumBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            e.Handled = (e.Key < Key.D0 || e.Key > Key.D9) && (e.Key < Key.NumPad0 || e.Key > Key.NumPad9);
        }
    }
}