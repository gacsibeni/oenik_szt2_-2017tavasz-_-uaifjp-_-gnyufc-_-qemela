﻿// <copyright file="TakarekkartyaWindow.xaml.cs" company="BBK Software">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SafeMoney.Ugyfel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using BusinessLogic.ViewModelek;
    using DataAccessLayer;

    /// <summary>
    /// Interaction logic for TakarekkartyaWindow.xaml
    /// </summary>
    public partial class TakarekkartyaWindow : Window
    {
        /// <summary>
        /// ViewModel
        /// </summary>
        private TakarekkartyaViewModel tkv;

        /// <summary>
        /// Initializes a new instance of the <see cref="TakarekkartyaWindow"/> class.
        /// Létrehoz egy új <see cref="TakarekkartyaWindow"/> példányt.
        /// </summary>
        /// <param name="ugyfel">Ügyfél aki nyitja az ablakot.</param>
        /// <param name="entities">Szülőablaktól kapott entities.</param>
        public TakarekkartyaWindow(UGYFEL ugyfel, DatabaseEntities entities)
        {
            this.InitializeComponent();
            this.tkv = new TakarekkartyaViewModel(ugyfel, entities);
            this.DataContext = this.tkv;
        }

        /// <summary>
        /// Takarékkártya igénylése
        /// </summary>
        /// <param name="sender">Nem használt</param>
        /// <param name="e">Nem használt.</param>
        private void TakarekkartyaIgenyeles(object sender, RoutedEventArgs e)
        {
            if (this.tkv.TakarekkartyaState != true)
            {
                this.tkv.TakarekkartyaState = true;
                this.tkv.SzamlaRepository.ChangeTakarekKartya(this.tkv.Ugyfel.SZAMLASZAM);
                this.tkv.Keret = this.tkv.Ugyfel.SZAMLA.EGYENLEG.Value / 2;
            }
        }

        /// <summary>
        /// Takarékkártya lemondása
        /// </summary>
        /// <param name="sender">Nem használt</param>
        /// <param name="e">Nem használt.</param>
        private void TakarékkártyaLemondasa(object sender, RoutedEventArgs e)
        {
            if (this.tkv.TakarekkartyaState == true)
            {
                this.tkv.TakarekkartyaState = false;
                this.tkv.SzamlaRepository.ChangeTakarekKartya(this.tkv.Ugyfel.SZAMLASZAM);
                this.tkv.Keret = 0;
            }
        }

        /// <summary>
        /// Fizetés takarékkártyán keresztül
        /// </summary>
        /// <param name="sender">Nem használt</param>
        /// <param name="e">Nem használt.</param>
        private void Fizetes(object sender, RoutedEventArgs e)
        {
            if (this.tkv.TakarekkartyaState == true)
            {
                int osszeg = int.Parse(this.SumBox.Text);

                if (osszeg <= this.tkv.Keret)
                {
                    this.tkv.SzamlaRepository.ChangeFunds(this.tkv.Ugyfel.SZAMLASZAM, false, osszeg);
                    this.tkv.Keret -= osszeg;
                }
                else
                {
                    MessageBox.Show("Érvénytelen összeg!");
                }
            }
            else
            {
                MessageBox.Show("HIBA: Önnek nincs takarékkártyája!");
            }
        }

        private void SumBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            e.Handled = (e.Key < Key.D0 || e.Key > Key.D9) && (e.Key < Key.NumPad0 || e.Key > Key.NumPad9);
        }
    }
}
