﻿// <copyright file="UgyfelMainWindow.xaml.cs" company="BBK Software">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SafeMoney
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using BusinessLogic;
    using DataAccessLayer;
    using SafeMoney.Ugyfel;

    /// <summary>
    /// Interaction logic for UgyfelMainWindow.xaml
    /// </summary>
    public partial class UgyfelMainWindow : Window
    {
        private UgyfelViewModel uV;

        /// <summary>
        /// Initializes a new instance of the <see cref="UgyfelMainWindow"/> class.
        /// Létrehoz egy  <see cref="UgyfelMainWindow"/> példánt.
        /// </summary>
        /// <param name="felhasznalo">Ügyfél aki belépett sikeresen a loginból.</param>
        /// <param name="entities">Loginwindowtól kapott entities.</param>
        public UgyfelMainWindow(UGYFEL felhasznalo, DatabaseEntities entities)
        {
            this.InitializeComponent();
            this.uV = new UgyfelViewModel(felhasznalo, entities);
            this.DataContext = this.uV;
            /*UV.UgyfelSzamlaja = (from SZAMLA sz in UV.SzamlaRepository.GetAll()
                              where sz.SZAMLASZAM == felhasznalo.SZAMLASZAM
                              select sz).Single();*/
        }

        private void Atutalas(object sender, RoutedEventArgs e)
        {
            AtutalasWindow aw = new AtutalasWindow(this.uV.Ugyfel, this.uV.Entities);
            aw.ShowDialog();
        }

        private void Befizetes(object sender, RoutedEventArgs e)
        {
            BefizetesWindow bfw = new BefizetesWindow(this.uV.Ugyfel, this.uV.Entities);
            bfw.ShowDialog();
        }

        private void Takarekkartya(object sender, RoutedEventArgs e)
        {
            TakarekkartyaWindow tkw = new TakarekkartyaWindow(this.uV.Ugyfel, this.uV.Entities);
            tkw.ShowDialog();
        }

        private void Lekotes(object sender, RoutedEventArgs e)
        {
            LekotesWindow lkw = new LekotesWindow(this.uV.Ugyfel, this.uV.Entities);
            lkw.ShowDialog();
        }
    }
}
