﻿// <copyright file="MainWindow.xaml.cs" company="BBK Software">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SafeMoney
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Navigation;
    using System.Windows.Shapes;
    using BusinessLogic;
    using DataAccessLayer;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private LoginViewModel lvm;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// Létrehoz egy <see cref="MainWindow"/> példányt.
        /// </summary>
        public MainWindow()
        {
            this.InitializeComponent();

            this.lvm = new LoginViewModel();
            this.DataContext = this.lvm;
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
        }

        private void Bejelentkezes(object sender, RoutedEventArgs e)
        {
            if (this.lvm.SelectedType == LoginViewModel.AccountType.Boss)
            {
                try
                {
                    FONOK fonok = (from FONOK f in this.lvm.FonokRepository.GetAll()
                                   where f.USERNAME == this.username.Text && f.PW == this.password.Password
                                   select f).Single();

                    if (fonok != null)
                    {
                        this.Visibility = Visibility.Hidden;
                        FonokMainWindow fmw = new FonokMainWindow();
                        fmw.ShowDialog();
                        this.Visibility = Visibility.Visible;
                    }
                }
                catch (InvalidOperationException)
                {
                    MessageBox.Show("Helytelen belépési adatok!");
                }
            }

            if (this.lvm.SelectedType == LoginViewModel.AccountType.Employee)
            {
                try
                {
                    UGYINTEZO ugyintezo = (from UGYINTEZO f in this.lvm.UgyintezoRepository.GetAll()
                                   where f.USERNAME == this.username.Text && f.PW == this.password.Password
                                   select f).Single();

                    if (ugyintezo != null)
                    {
                        this.Visibility = Visibility.Hidden;
                        UgyintezoMainWindow umw = new UgyintezoMainWindow();
                        umw.ShowDialog();
                        this.Visibility = Visibility.Visible;
                    }
                }
                catch (InvalidOperationException)
                {
                    MessageBox.Show("Helytelen belépési adatok!");
                }
            }

            if (this.lvm.SelectedType == LoginViewModel.AccountType.Client)
            {
                try
                {
                    UGYFEL ugyfel = (from UGYFEL uf in this.lvm.UgyfelRepository.GetAll()
                                   where uf.USERNAME == this.username.Text && uf.PW == this.password.Password
                                   select uf).Single();

                    if (ugyfel != null)
                    {
                        this.Visibility = Visibility.Hidden;
                        UgyfelMainWindow ufmw = new UgyfelMainWindow(ugyfel, this.lvm.Entities);
                        ufmw.ShowDialog();
                        this.Visibility = Visibility.Visible;
                    }
                }
                catch (InvalidOperationException)
                {
                    MessageBox.Show("Helytelen belépési adatok!");
                }
            }
        }
    }
}
