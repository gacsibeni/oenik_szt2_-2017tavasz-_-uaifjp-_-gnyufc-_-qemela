﻿using BusinessLogic.Converterek;
using DataAccessLayer;
using DataAccessLayer.Repositories;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Teszt
{
    [TestFixture]
    public class BLTests
    {

        BoolNegaterConverter bnconverter;

        [OneTimeSetUp]
        public void Setup()
        {
            bnconverter = new BoolNegaterConverter();
        }

        [Test]
        [TestCase(true)]
        [TestCase(false)]
        public void BoolNegaterConverterWorks(bool input)
        {
            bool output = (bool)bnconverter.Convert(input, typeof(bool), null, System.Globalization.CultureInfo.CurrentCulture);
            Assert.That(output.Equals(!input));
        }
    }
}
