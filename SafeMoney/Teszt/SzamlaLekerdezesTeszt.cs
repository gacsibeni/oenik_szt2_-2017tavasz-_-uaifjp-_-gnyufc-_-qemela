﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Moq;
using DataAccessLayer.Interfaces;
using DataAccessLayer;
using BusinessLogic.Lekerdezesek;

namespace Teszt
{
    class SzamlaLekerdezesTeszt
    {
        Mock<ISzamlaRepository> m;
        SzamlaLekerdezesek sz;

        [SetUp]
        public void Setup()
        {
            m = new Mock<ISzamlaRepository>();
            List<SZAMLA> l = new List<SZAMLA>()
            {
                new SZAMLA()
                {
                    TULAJDONOS= "Kiss Béla",
                    SZAMLASZAM = 140005,
                    EGYENLEG = 150000
                },
                new SZAMLA()
                {
                    TULAJDONOS= "Kiss Béla",
                    SZAMLASZAM = 140006,
                    EGYENLEG = 500000
                },
            };
            m.Setup(x => x.GetAll()).Returns(l.AsQueryable<SZAMLA>);
            sz = new SzamlaLekerdezesek(m.Object);
        }

        [Test]
        public void SzamlaSzamKeresTeszt()
        {
            Assert.That(sz.SzamlaszamraKeres(140006).ElementAt(0).TULAJDONOS, Is.EqualTo("Kiss Béla"));
            Assert.That(sz.SzamlaszamraKeres(140005).ElementAt(0).EGYENLEG, Is.EqualTo(150000));
        }

    }
}
