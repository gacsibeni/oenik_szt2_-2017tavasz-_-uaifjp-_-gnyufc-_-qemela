﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Moq;
using DataAccessLayer.Interfaces;
using DataAccessLayer;
using BusinessLogic.Lekerdezesek;

namespace Teszt
{
    class UgyfelLekerdezesTeszt
    {
        Mock<IUgyfelRepository> m;
        UgyfelLekerdezesek u;

        [SetUp]
        public void Setup()
        {
            m = new Mock<IUgyfelRepository>();
            List<UGYFEL> l = new List<UGYFEL>()
            {
                new UGYFEL()
                {
                    ID = 20001,
                    NEV = "Nagy Lajos",
                    SZAMLASZAM = 140005
                },
                new UGYFEL()
                {
                    ID = 20002,
                    NEV = "Cipész Margit",
                    SZAMLASZAM = 140007
                }
            };
            m.Setup(x => x.GetAll()).Returns(l.AsQueryable<UGYFEL>);
            u = new UgyfelLekerdezesek(m.Object);
        }

        [Test]
        public void UgyfelNevreKeresTeszt()
        {
            Assert.That(u.UgyfelNevreKeres("Cipész Margit").ElementAt(0).SZAMLASZAM, Is.EqualTo(140007));
            Assert.That(u.UgyfelNevreKeres("Nagy Lajos").ElementAt(0).ID, Is.EqualTo(20001));
        }
    }
}
