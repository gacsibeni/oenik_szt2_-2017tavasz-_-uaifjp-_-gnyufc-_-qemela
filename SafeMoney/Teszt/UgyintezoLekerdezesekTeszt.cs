﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Moq;
using DataAccessLayer.Interfaces;
using DataAccessLayer;
using BusinessLogic.Lekerdezesek;

namespace Teszt
{
    class UgyintezoLekerdezesekTeszt
    {
        Mock<IUgyinzetoRepository> m;
        UgyintezoLekerdezesek u;

        [SetUp]
        public void Setup()
        {
            m = new Mock<IUgyinzetoRepository>();
            List<UGYINTEZO> l = new List<UGYINTEZO>()
            {
                new UGYINTEZO()
                {
                    ID = 50001,
                    NEV = "Nagy Erzsébet",
                    USERNAME = "Erzsi"
                },
                new UGYINTEZO()
                {
                    ID = 50002,
                    NEV = "Kovács Péter",
                    USERNAME= "Peter"
                }
            };
            m.Setup(x => x.GetAll()).Returns(l.AsQueryable);
            u = new UgyintezoLekerdezesek(m.Object);
        }

        [Test]
        public void UgyintezoNevrekeresTeszt()
        {
            Assert.That(u.UgyintezoNevreKeres("Nagy Erzsébet").ElementAt(0).ID, Is.EqualTo(50001));
            Assert.That(u.UgyintezoNevreKeres("Kovács Péter").ElementAt(0).USERNAME, Is.EqualTo("Peter"));
        }
    }
}
