﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Moq;
using DataAccessLayer.Interfaces;
using DataAccessLayer;
using BusinessLogic.Lekerdezesek;

namespace Teszt
{
    class TranzakcioLekerdezesTeszt
    {
        Mock<ITranzakcioRepository> m;
        TranzakcioLekerdezesek t;
        [SetUp]
        public void Setup()
        {
            m = new Mock<ITranzakcioRepository>();
            List<TRANZAKCIO> l = new List<TRANZAKCIO>()
            {
                new TRANZAKCIO()
                {
                    ID = 30001,
                    OSSZEG = 15000,
                    CEL = 10002
                },
                new TRANZAKCIO()
                {
                    ID = 30002,
                    OSSZEG = 350000,
                    CEL = 10001
                }
            };
            m.Setup(x => x.GetAll()).Returns(l.AsQueryable<TRANZAKCIO>);
            t = new TranzakcioLekerdezesek(m.Object);       
        }

        [Test]
        public void TranzakcioIdraKeresTeszt()
        {
            Assert.That(t.TranzakcioIDraKeres(30002).ElementAt(0).OSSZEG, Is.EqualTo(350000));
            Assert.That(t.TranzakcioIDraKeres(30001).ElementAt(0).CEL, Is.EqualTo(10002));
        }
    }
}
