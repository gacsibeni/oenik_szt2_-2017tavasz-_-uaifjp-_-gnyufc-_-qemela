﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Moq;
using DataAccessLayer.Interfaces;
using DataAccessLayer;
using BusinessLogic.Lekerdezesek;


namespace Teszt
{
    [TestFixture]
    public class FonokLekerdTeszt
    {
        Mock<IFonokRepository> m;
        FonokLekredezesek f;

        [SetUp]
        public void Setup()
        {
            m = new Mock<IFonokRepository>();
            List<FONOK> l = new List<FONOK>()
            {
                new FONOK()
                {
                    NEV = "Kiss Józsi",
                    ID = 1001,
                    PW = "jozsi",
                    USERNAME = "Jozsi"
                },
                new FONOK()
                {
                    NEV = "Nagy Bela",
                    ID = 1002,
                    PW = "bela",
                    USERNAME = "Bela"
                }
            };
            m.Setup(x => x.GetAll()).Returns(l.AsQueryable<FONOK>);
            f = new FonokLekredezesek(m.Object);
        }

        [Test]
        public void NevreKeresTeszt()
        {
            Assert.That(f.NevreKeres("Kiss Józsi").ElementAt(0).NEV, Is.EqualTo("Kiss Józsi"));
            Assert.That(f.NevreKeres("Kiss Józsi").ElementAt(0).ID, Is.EqualTo(1001));            
        }

    }
}
