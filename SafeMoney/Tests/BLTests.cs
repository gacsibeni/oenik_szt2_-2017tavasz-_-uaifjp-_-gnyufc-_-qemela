﻿using BusinessLogic.Converterek;
using DataAccessLayer;
using DataAccessLayer.Repositories;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests
{
    [TestFixture]
    public class BLTests
    {

        BoolNegaterConverter bnconverter;

        [OneTimeSetUp]
        void Setup()
        {
            bnconverter = new BoolNegaterConverter();
        }

        [Test]
        [TestCase(true)]
        [TestCase(false)]
        public void BoolNegaterConverterWorks(bool input)
        {
            bool output = (bool)bnconverter.Convert(input, null, null, null);
            Assert.That(output.Equals(!input));
        }
    }
}
