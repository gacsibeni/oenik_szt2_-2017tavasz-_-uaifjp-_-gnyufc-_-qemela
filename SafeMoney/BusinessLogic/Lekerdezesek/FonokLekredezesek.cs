﻿// <copyright file="FonokLekredezesek.cs" company="BBK-Software">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BusinessLogic.Lekerdezesek
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DataAccessLayer;
    using DataAccessLayer.Interfaces;

    /// <summary>
    /// Class for the FonokRepository objects, containing methods to be used for data accessing.
    /// </summary>
    public class FonokLekredezesek
    {
        private IFonokRepository repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="FonokLekredezesek"/> class.
        /// Létrehoz egy <see cref="FonokLekredezesek"/> osztálypéldányt.
        /// </summary>
        /// <param name="repo">A repo példány amelyen a metódusok futnak.</param>
        public FonokLekredezesek(IFonokRepository repo)
        {
            this.repo = repo;
        }

        /// <summary>
        /// IQueryable objektumot ad vissza név alapján.
        /// </summary>
        /// <param name="nev">The name string to be searched in the repository.</param>
        /// <returns>Returns IQueryable type that contains the FONOK object if the search was successful.</returns>
        public IQueryable<FONOK> NevreKeres(string nev)
        {
            var vissza = from x in this.repo.GetAll()
                         where x.NEV == nev
                         select x;
            return vissza;
        }
    }
}
