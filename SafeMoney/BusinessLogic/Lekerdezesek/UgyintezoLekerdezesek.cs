﻿// <copyright file="UgyintezoLekerdezesek.cs" company="BBK-Software">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BusinessLogic.Lekerdezesek
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DataAccessLayer;
    using DataAccessLayer.Interfaces;

    /// <summary>
    /// Ügyintáző repóhoz tartozó metódusok.
    /// </summary>
    public class UgyintezoLekerdezesek
    {
        private IUgyinzetoRepository repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="UgyintezoLekerdezesek"/> class.
        /// Új <see cref="UgyintezoLekerdezesek"/> osztálypéldány.
        /// </summary>
        /// <param name="repo">A repository amin a metódusok futnak. </param>
        public UgyintezoLekerdezesek(IUgyinzetoRepository repo)
        {
            this.repo = repo;
        }

        /// <summary>
        /// Ügyintézőkre keresés név alapján.
        /// </summary>
        /// <param name="nev">Keresendő név.</param>
        /// <returns>Talált ügyintézők IQueryable típusban.</returns>
        public IQueryable<UGYINTEZO> UgyintezoNevreKeres(string nev)
        {
            var vissza = from x in this.repo.GetAll()
                         where x.NEV == nev
                         select x;
            return vissza;
        }
    }
}
