﻿// <copyright file="UgyfelLekerdezesek.cs" company="BBK-Software">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BusinessLogic.Lekerdezesek
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DataAccessLayer;
    using DataAccessLayer.Interfaces;

    /// <summary>
    /// Ügyféllekérdezések osztálya
    /// </summary>
    public class UgyfelLekerdezesek
    {
        private IUgyfelRepository repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="UgyfelLekerdezesek"/> class.
        /// </summary>
        /// <param name="repo">Repository the methods to be ran on.</param>
        public UgyfelLekerdezesek(IUgyfelRepository repo)
        {
            this.repo = repo;
        }

        /// <summary>
        /// Ügyfélre keres név alapján.
        /// </summary>
        /// <param name="nev">Keresendő név.</param>
        /// <returns>IQueryable formában visszatér a találatokkal.</returns>
        public IQueryable<UGYFEL> UgyfelNevreKeres(string nev)
        {
            var vissza = from x in this.repo.GetAll()
                         where x.NEV == nev
                         select x;
            return vissza;
        }
    }
}
