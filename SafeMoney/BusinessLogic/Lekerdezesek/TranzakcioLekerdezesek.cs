﻿// <copyright file="TranzakcioLekerdezesek.cs" company="BBK-Software">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BusinessLogic.Lekerdezesek
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DataAccessLayer;
    using DataAccessLayer.Interfaces;

    /// <summary>
    /// Osztálymetódusok Tranzakciólekérdesékhez.
    /// </summary>
    public class TranzakcioLekerdezesek
    {
        private ITranzakcioRepository repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="TranzakcioLekerdezesek"/> class.
        /// Létrehoz egy új <see cref="TranzakcioLekerdezesek"/> példányt.
        /// </summary>
        /// <param name="repo">A repository amin a metódusok futnak.</param>
        public TranzakcioLekerdezesek(ITranzakcioRepository repo)
        {
            this.repo = repo;
        }

        /// <summary>
        /// Tranzakcióra keres ID alapján.
        /// </summary>
        /// <param name="id">Keresendő ID.</param>
        /// <returns>IQueryable formában a talált Tranzakciók.</returns>
        public IQueryable<TRANZAKCIO> TranzakcioIDraKeres(decimal id)
        {
            var vissza = from x in this.repo.GetAll()
                         where x.ID == id
                         select x;
            return vissza;
        }
    }
}
