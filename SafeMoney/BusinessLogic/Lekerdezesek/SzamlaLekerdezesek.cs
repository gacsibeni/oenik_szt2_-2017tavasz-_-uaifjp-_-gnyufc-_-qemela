﻿// <copyright file="SzamlaLekerdezesek.cs" company="BBK-Software">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BusinessLogic.Lekerdezesek
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DataAccessLayer;
    using DataAccessLayer.Interfaces;

    /// <summary>
    /// SzamlaLekerdezesek class for SZAMLA data accessing methods.
    /// </summary>
    public class SzamlaLekerdezesek
    {
        private ISzamlaRepository repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="SzamlaLekerdezesek"/> class.
        /// Létrehoz egy <see cref="SzamlaLekerdezesek"/> osztálypéldányt.
        /// </summary>
        /// <param name="repo">A repo amin a metódusok futnak.</param>
        public SzamlaLekerdezesek(ISzamlaRepository repo)
        {
            this.repo = repo;
        }

        /// <summary>
        /// Számlára keres számlaszám alapján.
        /// </summary>
        /// <param name="szamlaszam">Keresendő számlaszám.</param>
        /// <returns>Visszatér a talált számlákkal IQUeryable formában.</returns>
        public IQueryable<SZAMLA> SzamlaszamraKeres(decimal szamlaszam)
        {
            var vissza = from x in this.repo.GetAll()
                         where x.SZAMLASZAM == szamlaszam
                         select x;
            return vissza;
        }
    }
}
