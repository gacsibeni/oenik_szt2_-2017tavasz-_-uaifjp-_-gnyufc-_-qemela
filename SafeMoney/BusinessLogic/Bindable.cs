﻿// <copyright file="Bindable.cs" company="BBK-Software">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BusinessLogic
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;

    /// <summary>
    /// Azt teszi lehetővé, hogy az adatközéssel létrehozott grafikánk frissüljön.
    /// </summary>
    public class Bindable : INotifyPropertyChanged
    {
        /// <summary>
        /// Erre az eventre iratkoznak fel az egyes objektumok az XML-ben
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// A metódus meghívásával hívjuk megaz eventünket, a megfelelő paraméterekkel.
        /// </summary>
        /// <param name="name">A frissítendő objektum neve</param>
        protected void OnPropertyChanged(string name = null)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        /// <summary>
        /// Paraméteradásnál fontos, hog ha ezzel adjuk meg, az adatkötés által frissül.
        /// </summary>
        /// <typeparam name="T">Típus megadás</typeparam>
        /// <param name="field">Minek az értékét változtatjuk</param>
        /// <param name="newvalue">Mivé változtatjuk</param>
        /// <param name="name">A frissítendő objektum neve</param>
        protected void SetProperty<T>(ref T field, T newvalue, [CallerMemberName] string name = null)
        {
            field = newvalue;
            this.OnPropertyChanged(name);
        }
    }
}