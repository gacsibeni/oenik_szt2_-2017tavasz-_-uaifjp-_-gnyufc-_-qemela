﻿// <copyright file="DiagramErtekPar.cs" company="BBK Software">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BusinessLogic.FonokLekerdezesek
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Diagramhoz értékpár adatokat tárol
    /// </summary>
    public class DiagramErtekPar
    {
        /// <summary>
        /// Gets or sets Idopont
        /// </summary>
        public DateTime Idopont { get; set; }

        /// <summary>
        /// Gets or sets Ertek
        /// </summary>
        public double Ertek { get; set; }
    }
}
