﻿// <copyright file="AdozasMetodusok.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BusinessLogic.FonokLekerdezesek
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;
    using DataAccessLayer;

    /// <summary>
    /// Adózás metódusai
    /// </summary>
    public class AdozasMetodusok
    {
        /// <summary>
        /// Kimutatást generáló metódus
        /// </summary>
        /// <param name="indulo">Induló dátum</param>
        /// <param name="vegso">Végső dátum</param>
        /// <param name="fvm">FonokViewModel referencia</param>
        public static void KimutatasGeneralas(DateTime indulo, DateTime vegso, FonokViewModel fvm)
        {
            double forgalom = OsszesForgalomDatumKozott(indulo, vegso, fvm);
            double ujPenzek = FrissSzamlaPenzekDatumKozott(indulo, vegso, fvm);
            string path = string.Empty;

            FolderBrowserDialog fw = new FolderBrowserDialog();
            if (fw.ShowDialog() == DialogResult.OK)
            {
                path = fw.SelectedPath;
            }

            string jelentes = string.Format("A vállalat adózási kimutatása a kijelölt időpontban : {0} {1}\n A vállalat forgalma: {2}\n A vállalat új vagyona: {3}", indulo.Date, vegso.Date, forgalom, ujPenzek);

            StreamWriter sw = new StreamWriter(path + "\\jelentes.txt");
            sw.Write(jelentes);
            sw.Close();
        }

        private static double OsszesForgalomDatumKozott(DateTime indulo, DateTime vegso, FonokViewModel fvm)
        {
            double osszeg = 0;
            foreach (TRANZAKCIO t in fvm.Tranzakciok)
                {
                if (t.DATUM >= indulo && t.DATUM <= vegso)
                {
                    osszeg += (double)t.OSSZEG;
                }
            }

            return osszeg;
        }

        private static double FrissSzamlaPenzekDatumKozott(DateTime indulo, DateTime vegso, FonokViewModel fvm)
        {
            double osszeg = 0;

            foreach (SZAMLA i in fvm.Szamlak)
            {
                if (i.STARTDATE >= indulo && i.STARTDATE <= vegso)
                {
                    osszeg += (double)i.EGYENLEG;
                }
            }

            return osszeg;
        }
    }
}
