﻿// <copyright file="DiagramMetodusok.cs" company="BBK Software">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BusinessLogic.FonokLekerdezesek
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DataAccessLayer;

    /// <summary>
    /// Diagramhoz szükséges metódusok
    /// </summary>
    public class DiagramMetodusok
    {
        private FonokViewModel fvm;
        private List<DateTime> datumok;

        /// <summary>
        /// Initializes a new instance of the <see cref="DiagramMetodusok"/> class.
        /// </summary>
        /// <param name="fvm">FonokViewModel referencia</param>
        /// <param name="datumok">Dátumok listája</param>
        public DiagramMetodusok(FonokViewModel fvm, List<DateTime> datumok)
        {
            this.fvm = fvm;
            this.datumok = datumok;
        }

        /// <summary>
        /// Dátumok listáját tölti fel
        /// </summary>
        /// <param name="l">Lista referencia</param>
        /// <param name="fvm">FonokViewModel referencia</param>
        public static void DatumokFeltolt(List<DateTime> l, FonokViewModel fvm)
        {
            l.Add(new DateTime(2017, 01, 01));

            var res = from x in fvm.Szamlak
                      select x.STARTDATE;
            foreach (DateTime d in res)
            {
                l.Add(d);
            }
        }

        /// <summary>
        /// Dátumokhoz párosít egyenleget
        /// </summary>
        /// <param name="d">Dátum</param>
        /// <returns>Egyenleg</returns>
        public double DatumParosit(DateTime d)
        {
            var res = from x in this.fvm.Szamlak
                      where x.STARTDATE == d
                      select x.EGYENLEG;

            double seged = 0;
            if (res.Count() > 0)
            {
                seged = (double)res.ElementAt(0);
            }

            seged += this.KorabbiOsszeg(d);
            return seged;
        }

        private double KorabbiOsszeg(DateTime d)
        {
            var res = from x in this.fvm.Szamlak
                      where x.STARTDATE < d
                      select x;
            double vissza = 0;
            if (res.Count() > 0)
            {
                foreach (SZAMLA i in res)
                {
                    vissza += (double)i.EGYENLEG;
                }
            }

            return vissza;
        }
    }
}
