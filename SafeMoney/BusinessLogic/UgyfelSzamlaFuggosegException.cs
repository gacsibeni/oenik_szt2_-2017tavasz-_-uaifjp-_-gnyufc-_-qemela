﻿//-----------------------------------------------------------------------
//-----------------------------------------------------------------------
// <copyright file="UgyfelSzamlaFuggosegException.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BusinessLogic.Kivetelek
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Ugyfelszamlafuggoseg kivetel
    /// </summary>
    public class UgyfelSzamlaFuggosegException : ApplicationException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UgyfelSzamlaFuggosegException"/> class.
        /// </summary>
        /// <param name="message">Üzenet</param>
        public UgyfelSzamlaFuggosegException(string message)
            : base(message)
        {
        }
    }
}
