﻿// <copyright file="BoolNegaterConverter.cs" company="BBK-Software">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BusinessLogic.Converterek
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Data;

    /// <summary>
    /// Konverter, amely negálja a kapott bool értéket a window számára.
    /// </summary>
    public class BoolNegaterConverter : IValueConverter
    {
        /// <summary>
        /// A konvertáló metódus megvalósítása.
        /// </summary>
        /// <param name="value">A kapott érték.</param>
        /// <param name="targetType">A konvertálás céltípusa.</param>
        /// <param name="parameter">Objektum paraméter.</param>
        /// <param name="culture">Jelenlegi kultúrakörnyezet.</param>
        /// <returns>Átkonvertált érték.</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return !(bool)value;
        }

        /// <summary>
        /// A visszakonvertáló medótus. Megvalósítására nincs szükség.
        /// </summary>
        /// <param name="value">N/A</param>
        /// <param name="targetType">Nem használt.</param>
        /// <param name="parameter">Nem szükséges.</param>
        /// <param name="culture">Nem kell.</param>
        /// <returns>Kivétel.</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
