﻿// <copyright file="BoolToSzovegConverter.cs" company="BBK-Software">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BusinessLogic.Converterek
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Data;

    /// <summary>
    /// Bool értéket stringgé konvertáló osztály megvalósítésa a window számára. Takarékkártya ablak használja.
    /// </summary>
    public class BoolToSzovegConverter : IValueConverter
    {
        /// <summary>
        /// Boolból stringet generáló metódus.
        /// </summary>
        /// <param name="value">Beérkező bool.</param>
        /// <param name="targetType">Céltípus.</param>
        /// <param name="parameter">Objektum paraméter.</param>
        /// <param name="culture">Kultúrainformációk.</param>
        /// <returns>Kimenet: string.</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((bool)value)
            {
                return "Önnek van takarékkártyája.";
            }
            else
            {
                return "Önnek nincsen takarékkártyája.";
            }
        }

        /// <summary>
        /// A visszakonvertálás nem megvalósított.
        /// </summary>
        /// <param name="value">N/A</param>
        /// <param name="targetType">Nem szükséges.</param>
        /// <param name="parameter">Nem megvalósított.</param>
        /// <param name="culture">Nem használt.</param>
        /// <returns>Kivétel</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
