﻿// <copyright file="UgyfelekListToStringListConverter.cs" company="BBK Software">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BusinessLogic.Converterek
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Data;
    using DataAccessLayer;

    /// <summary>
    /// Ügyfél listát stringgé konvertáló konverter.
    /// </summary>
    public class UgyfelekListToStringListConverter : IValueConverter
    {
        /// <summary>
        /// A konvertáló metódus.
        /// </summary>
        /// <param name="value">Bemeneti ügyféllista.</param>
        /// <param name="targetType">Cél típus.</param>
        /// <param name="parameter">Objektum paraméter.</param>
        /// <param name="culture">Kultúrainfo.</param>
        /// <returns>Visszaad stringet.</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            ObservableCollection<UGYFEL> ugyfelek = (ObservableCollection<UGYFEL>)value;
            List<string> ugyfelekString = new List<string>();
            foreach (UGYFEL item in ugyfelek)
            {
                ugyfelekString.Add(item.NEV);
            }

            return ugyfelekString;
        }

        /// <summary>
        /// Nem megvalósított a ConvertBack.
        /// </summary>
        /// <param name="value">N/A</param>
        /// <param name="targetType">Nem használt.</param>
        /// <param name="parameter">Nem szükséges.</param>
        /// <param name="culture">Nem megvalósított.</param>
        /// <returns>Kivétel.</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
