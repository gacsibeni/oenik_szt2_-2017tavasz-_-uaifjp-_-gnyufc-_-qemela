﻿// <copyright file="TakarekkartyaViewModel.cs" company="BBK Software">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BusinessLogic.ViewModelek
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Text;
    using System.Threading.Tasks;
    using DataAccessLayer;
    using DataAccessLayer.Repositories;

    /// <summary>
    /// A takarékkártya ablak VM osztálya.
    /// </summary>
    public class TakarekkartyaViewModel : INotifyPropertyChanged
    {
        private bool takarekkartyaState;

        private decimal keret;

        /// <summary>
        /// Initializes a new instance of the <see cref="TakarekkartyaViewModel"/> class.
        /// Létrehoz egy <see cref="TakarekkartyaViewModel"/> példányt.
        /// </summary>
        /// <param name="ugyfel">Ugyfel érték.</param>
        /// <param name="entities">Entity érték.</param>
        public TakarekkartyaViewModel(UGYFEL ugyfel, DatabaseEntities entities)
        {
            this.Entities = entities;
            this.SzamlaRepository = new SzamlaRepository(entities);
            this.Ugyfel = ugyfel;
            this.TakarekkartyaState = (bool)this.Ugyfel.SZAMLA.TAKAREKKARTYA;
            if (this.TakarekkartyaState)
            {
                this.Keret = (decimal)this.Ugyfel.SZAMLA.EGYENLEG / 2;
            }
            else
            {
                this.Keret = 0;
            }
        }

        /// <summary>
        /// Propertychanged esemény.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets or sets a value indicating whether the current client has or does not have a bank deposit.
        /// Beállítja a Takarékkárytaállapot tulajdonság értékét, vagy lekéri.
        /// </summary>
        public bool TakarekkartyaState
        {
            get
            {
                return this.takarekkartyaState;
            }

            set
            {
                this.takarekkartyaState = value;
                this.OnPropChanged();
            }
        }

        /// <summary>
        /// Gets or sets the value of the deposit of the current client.
        /// Beállítja az ügyfélkeret tulajdonság értékét, vagy lekéri.
        /// </summary>
        public decimal Keret
        {
            get
            {
                return this.keret;
            }

            set
            {
                this.keret = value;
                this.OnPropChanged();
            }
        }

        /// <summary>
        /// Gets or sets the value of database entity used throughout this windows operations.
        /// Beállítja az Entities értékét, vagy lekéri.
        /// </summary>
        public DatabaseEntities Entities { get; set; }

        /// <summary>
        /// Gets or sets the value of SzamlaRepository, Szamlarepository is used by the window.
        /// Beállítja a SzamlaRepository értékét, vagy lekéri.
        /// </summary>
        public SzamlaRepository SzamlaRepository { get; set; }

        /// <summary>
        /// Gets or sets the value of the Ugyfel that opened up this window.
        /// Beállítja az Ugyfel értékét, vagy lekéri.
        /// </summary>
        public UGYFEL Ugyfel { get; set; }

        /// <summary>
        /// PropertyChanged esemény által indított metódus.
        /// </summary>
        /// <param name="n">Hívó tulajdonság neve.</param>
        public void OnPropChanged([CallerMemberName] string n = "")
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(n));
        }
    }
}
