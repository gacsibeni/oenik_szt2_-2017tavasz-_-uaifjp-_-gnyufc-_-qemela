﻿// <copyright file="BefizetesViewModel.cs" company="BBK Software">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BusinessLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DataAccessLayer;
    using DataAccessLayer.Repositories;

    /// <summary>
    /// Befizetés ablak Viewmodel osztálya.
    /// </summary>
    public class BefizetesViewModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BefizetesViewModel"/> class.
        /// Létrehoz egy új <see cref="BefizetesViewModel"/> példányt.
        /// </summary>
        /// <param name="entities">Az entity paraméter amellyel létrejön a VM. </param>
        public BefizetesViewModel(DatabaseEntities entities)
        {
            this.Entities = entities;

            this.SzamlaRepository = new SzamlaRepository(entities);
        }

        /// <summary>
        /// Gets or sets the database entity used by the window.
        /// Beállítja az Entities tulajdonság értékét.
        /// </summary>
        public DatabaseEntities Entities { get; set; }

        /// <summary>
        /// Gets or sets the SzamlaRepository used by the window.
        /// Beállítja a SzamlaRepository tulajdonság értékét.
        /// </summary>
        public SzamlaRepository SzamlaRepository { get; set; }
    }
}
