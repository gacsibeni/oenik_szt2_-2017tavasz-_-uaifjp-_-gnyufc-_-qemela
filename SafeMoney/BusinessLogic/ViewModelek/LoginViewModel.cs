﻿// <copyright file="LoginViewModel.cs" company="BBK-Software">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BusinessLogic
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DataAccessLayer;
    using DataAccessLayer.Repositories;

    /// <summary>
    /// A főablak, a loginablak viewmodel osztálya.
    /// </summary>
    public class LoginViewModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LoginViewModel"/> class.
        /// Kreál egy <see cref="LoginViewModel"/> osztálypéldányt.
        /// </summary>
        public LoginViewModel()
        {
            this.Entities = new DatabaseEntities();

            this.FonokRepository = new FonokRepository(this.Entities);

            this.UgyintezoRepository = new UgyintezoRepository(this.Entities);

            this.UgyfelRepository = new UgyfelRepository(this.Entities);

            this.AccountTypes = new ObservableCollection<AccountType>();
            this.AccountTypes.Add(AccountType.Boss);
            this.AccountTypes.Add(AccountType.Client);
            this.AccountTypes.Add(AccountType.Employee);
        }

        /// <summary>
        /// Létrehoz egy AccountType enumot amely az azonosításhoz szükséges.
        /// </summary>
        public enum AccountType
        {
            /// <summary>
            /// Főnök bejelentkezés.
            /// </summary>
            Boss,

            /// <summary>
            /// Ügyintéző login.
            /// </summary>
            Employee,

            /// <summary>
            /// Ügyfél login.
            /// </summary>
            Client
        }

        /// <summary>
        /// Gets or sets the value of a database entity that is used throughout the program for DA.
        /// Beállítja az Entities értékét, vagy lekéri.
        /// </summary>
        public DatabaseEntities Entities { get; set; }

        /// <summary>
        /// Gets or sets the value of a FonokRepository, used to browse through the bosses while identifying.
        /// Beállítja a FonokRepository értékét, vagy lekéri.
        /// </summary>
        public FonokRepository FonokRepository { get; set; }

        /// <summary>
        /// Gets or sets the value of a FonokRepository, used to browse through the clients while identifying.
        /// Beállítja az UgyfelRepository értékét, vagy lekéri.
        /// </summary>
        public UgyfelRepository UgyfelRepository { get; set; }

        /// <summary>
        /// Gets or sets the value of a FonokRepository, used to browse through the employees while identifying.
        /// Beállítja az UgyintezoRepository értékét, vagy lekéri.
        /// </summary>
        public UgyintezoRepository UgyintezoRepository { get; set; }

        /// <summary>
        /// Gets or sets the value of an OC, that is used in the listbox to be chosen which type of account the user wants to log in to.
        /// Beállítja az AccountType megfigyelhető kollekció értékét, vagy lekéri.
        /// </summary>
        public ObservableCollection<AccountType> AccountTypes { get; set; }

        /// <summary>
        /// Gets or sets the value of the account type selected from the listbox.
        /// Beállítja a AT listboxban kiválasztott értékét, vagy lekéri.
        /// </summary>
        public AccountType SelectedType { get; set; }
    }
}
