﻿// <copyright file="UgyfelViewModel.cs" company="BBK-Software">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BusinessLogic
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Text;
    using System.Threading.Tasks;
    using DataAccessLayer;
    using DataAccessLayer.Repositories;

    /// <summary>
    /// Viewmodel class for UgyfelWindow.
    /// </summary>
    public class UgyfelViewModel : INotifyPropertyChanged
    {
        private decimal egyenleg;

        /// <summary>
        /// Initializes a new instance of the <see cref="UgyfelViewModel"/> class.
        /// Egy <see cref="UgyfelViewModel"/> osztálypéldány létrehozása.
        /// </summary>
        /// <param name="entities">Database entities to be used thourghout the operations of this window. Is passed on if child windows are opened from this one.</param>
        /// <param name="felhasznalo">Felhasznalo aki használja az ablakot.</param>
        public UgyfelViewModel(UGYFEL felhasznalo, DatabaseEntities entities)
        {
            this.Entities = entities;

            this.TranzakcioRepository = new TranzakcioRepository(entities);

            this.SzamlaRepository = new SzamlaRepository(entities);

            this.Szamlak = new ObservableCollection<SZAMLA>();

            this.Ugyfel = felhasznalo;

            this.Egyenleg = (decimal)this.Ugyfel.SZAMLA.EGYENLEG;

            var q = this.SzamlaRepository.GetAll();

            foreach (var x in q)
            {
                this.Szamlak.Add(x);
            }
        }

        /// <summary>
        /// PropChanged event viewer for the window to able to be react to changes in data.
        /// PropChanged eseménykezelő.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets or sets the value of SzamlaRepository.
        /// Beállítja az Entities tulajdonság értékét.
        /// </summary>
        public SzamlaRepository SzamlaRepository { get; set; }

        /// <summary>
        /// Gets or sets the value of TranzakcioRepository.
        /// Beállítja a TranzakcioRepository tulajdonság értékét.
        /// </summary>
        public TranzakcioRepository TranzakcioRepository { get; set; }

        /// <summary>
        /// Gets or sets the value of the database entities object.
        /// Beállítja az Entities tulajdonság értékét.
        /// </summary>
        public DatabaseEntities Entities { get; set; }

        /// <summary>
        /// Gets or sets the value of the client that opened up this window (successfully logged in from login window).
        /// Beállítja az Ugyfel tulajdonság értékét.
        /// </summary>
        public UGYFEL Ugyfel { get; set; }

        /// <summary>
        /// Gets or sets the value of an OC that is used to browse data in the window.
        /// Beállítja a Szamlak OC tulajdonság értékét.
        /// </summary>
        public ObservableCollection<SZAMLA> Szamlak { get; set; }

        /// <summary>
        /// Gets or sets the value of the client's balance
        /// Beállítja az ügyfél egyenlegét.
        /// </summary>
        public decimal Egyenleg
        {
            get
            {
                return this.egyenleg;
            }

            set
            {
                this.egyenleg = value;
                this.OnPropChanged();
            }
        }

        /// <summary>
        /// PropertyChanged esemény kiváltódásakor fut.
        /// </summary>
        /// <param name="n">Hívó példány neve.</param>
        public void OnPropChanged([CallerMemberName] string n = "")
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(n));
        }
    }
}
