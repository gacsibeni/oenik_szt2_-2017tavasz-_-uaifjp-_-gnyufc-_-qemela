﻿// <copyright file="UgyintezoViewModel.cs" company="BBK Software">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BusinessLogic
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BusinessLogic.Kivetelek;
    using BusinessLogic.Lekerdezesek;
    using DataAccessLayer;
    using DataAccessLayer.Repositories;

    /// <summary>
    /// UgyintezoViewModel
    /// </summary>
    public class UgyintezoViewModel : Bindable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UgyintezoViewModel"/> class.
        /// </summary>
        public UgyintezoViewModel()
        {
            this.Entities = new DatabaseEntities();
            this.UgyfelRepository = new UgyfelRepository(this.Entities);
            this.SzamlaRepository = new SzamlaRepository(this.Entities);
            this.TranzakcioRepository = new TranzakcioRepository(this.Entities);

            this.Szamlak = new ObservableCollection<SZAMLA>();
            this.Tranzakciok = new ObservableCollection<TRANZAKCIO>();
            this.Ugyfelek = new ObservableCollection<UGYFEL>();

            var q = from akt in this.SzamlaRepository.GetAll()
                    select akt;

            // using (DatabaseEntities context = new DatabaseEntities())  //ez kellett bele hogy ne szálljon el, valami EF tranziensjelenség okozza...
            foreach (var x in q)
            {
                this.Szamlak.Add(x);
            }

            var q2 = from akt in this.TranzakcioRepository.GetAll()
                     select akt;

            foreach (var x in q2)
            {
                this.Tranzakciok.Add(x);
            }

            var q3 = from akt in this.UgyfelRepository.GetAll()
                     select akt;

            foreach (var x in q3)
            {
                this.Ugyfelek.Add(x);
            }
        }

        /// <summary>
        /// Gets or sets Entities
        /// </summary>
        public DatabaseEntities Entities { get; set; }

        /// <summary>
        /// Gets or sets UgyfelReository
        /// </summary>
        public UgyfelRepository UgyfelRepository { get; set; }

        /// <summary>
        /// Gets or sets SzamlaRepository
        /// </summary>
        public SzamlaRepository SzamlaRepository { get; set; }

        /// <summary>
        /// Gets or sets TranzakcioRepository
        /// </summary>
        public TranzakcioRepository TranzakcioRepository { get; set; }

        /// <summary>
        /// Gets or sets Szamlak
        /// </summary>
        public ObservableCollection<SZAMLA> Szamlak { get; set; }

        /// <summary>
        /// Gets or sets Tranzakciok
        /// </summary>
        public ObservableCollection<TRANZAKCIO> Tranzakciok { get; set; }

        /// <summary>
        /// Gets or sets Ugyfelek
        /// </summary>
        public ObservableCollection<UGYFEL> Ugyfelek { get; set; }

        /// <summary>
        /// Gets or sets AktivSzamla
        /// </summary>
        public SZAMLA AktivSzamla { get; set; }

        /// <summary>
        /// Gets or sets AktivUgyfel
        /// </summary>
        public UGYFEL AktivUgyfel { get; set; }

        /// <summary>
        /// Kovetkezo szamla ID-ja
        /// </summary>
        /// <returns>ID</returns>
        public decimal KovetkezoSzamlaID()
        {
            return this.Szamlak.Max(x => x.SZAMLASZAM) + 1;
        }

        /// <summary>
        /// Kovetkezo ügyfél ID
        /// </summary>
        public void KovetkezoUgyfelaID()
        {
            this.AktivUgyfel.ID = this.Ugyfelek.Max(x => x.ID) + 1;
            this.OnPropertyChanged("AktivUgyfel.ID");
        }

        /// <summary>
        /// Számla másolása
        /// </summary>
        /// <param name="masolando">Másolandó</param>
        /// <param name="celszamla">Cél</param>
        public void SzamlaMasolas(SZAMLA masolando, SZAMLA celszamla)
        {
            celszamla.SZAMLASZAM = masolando.SZAMLASZAM;
            celszamla.EGYENLEG = masolando.EGYENLEG;
            celszamla.TULAJDONOS = masolando.TULAJDONOS;
        }

        /// <summary>
        /// Számlakeresés
        /// </summary>
        /// <param name="szam">Számlaszám</param>
        /// <returns>Eredmény</returns>
        public SZAMLA SzamlaKereses(decimal szam)
        {
            var vissza = (from x in this.SzamlaRepository.GetAll()
                          where x.SZAMLASZAM == szam
                          select x).SingleOrDefault();
            return vissza;
        }

        /// <summary>
        /// Ügyfél keresés
        /// </summary>
        /// <param name="keresendo">Keresendő név</param>
        public void UgyfelKeres(string keresendo)
        {
            this.Ugyfelek = new ObservableCollection<UGYFEL>();
            var q3 = from akt in this.UgyfelRepository.GetAll()
                     where akt.NEV.ToUpper().Contains(keresendo.ToUpper()) || keresendo == string.Empty
                     select akt;

            foreach (var x in q3)
            {
                this.Ugyfelek.Add(x);
            }

            this.OnPropertyChanged("Ugyfelek");
        }

        /// <summary>
        /// Ügyfél másolás
        /// </summary>
        /// <param name="masolando">Másolandó</param>
        /// <param name="celugyfel">Cél</param>
        public void UgyfelMasolas(UGYFEL masolando, UGYFEL celugyfel)
        {
            celugyfel.ID = masolando.ID;
            celugyfel.NEV = masolando.NEV;
            celugyfel.SZAMLA = masolando.SZAMLA;
            celugyfel.USERNAME = masolando.USERNAME;
            if (masolando.PW != string.Empty)
            {
                celugyfel.PW = masolando.PW;
            }
        }

        /// <summary>
        /// Számla törlés
        /// </summary>
        public void SzamlaTorles()
        {
            SZAMLA s = this.Szamlak.Single(x => x.SZAMLASZAM == this.AktivSzamla.SZAMLASZAM);
            foreach (var item in this.Ugyfelek)
            {
                if (item.SZAMLA == s)
                {
                    item.SZAMLA = null;
                }
            }

            this.Szamlak.Remove(s);
        }

        /// <summary>
        /// Ügyfél mentés és törlés ellenőrzés
        /// </summary>
        public void UgyfelMentesTorlesEllenorzes()
        {
            UGYFEL s = (UGYFEL)this.Ugyfelek.Single(x => x.ID == this.AktivUgyfel.ID);
            if (s.SZAMLA != null && s.SZAMLA.TULAJDONOS == s.NEV && ((s.SZAMLA == null && this.AktivUgyfel.SZAMLA == null) || s.SZAMLA != this.AktivUgyfel.SZAMLA))
            {
                this.AktivUgyfel.SZAMLA = s.SZAMLA;
                throw new UgyfelSzamlaFuggosegException(string.Format("A {0} számú számla tulajdonosának {1} van beregisztrálva. Előbb törölje a számlát.", s.SZAMLA.SZAMLASZAM, this.AktivUgyfel.NEV));
            }
        }

        /// <summary>
        /// Felhasználónév ellenőrzés
        /// </summary>
        /// <returns>Eredmény</returns>
        public bool UsernameEllenőrzés()
        {
            bool ret = this.Ugyfelek.SingleOrDefault(x => x.USERNAME.ToUpper() == this.AktivUgyfel.USERNAME.ToUpper()) == null;
            return ret;
        }

        /// <summary>
        /// Ugyfél név módosítás
        /// </summary>
        public void UgyfelNevModositasa()
        {
            try
            {
                UGYFEL s = (UGYFEL)this.Ugyfelek.Single(x => x.ID == this.AktivUgyfel.ID);
                if (s.SZAMLA.TULAJDONOS == s.NEV)
                {
                    s.SZAMLA.TULAJDONOS = this.AktivUgyfel.NEV;
                }
            }
            catch
            {
            }
        }

        /// <summary>
        /// Új ügyfél felvétel
        /// </summary>
        public void UjUgyfel()
        {
            this.Ugyfelek.Add(this.AktivUgyfel);
            this.UgyfelRepository.Insert(this.AktivUgyfel);
        }

        /// <summary>
        /// Ugyfél módosítás
        /// </summary>
        /// <param name="regiUgyfel">Ügyfél referencia</param>
        public void UgyfelModositas(UGYFEL regiUgyfel)
        {
            this.UgyfelMasolas(this.AktivUgyfel, regiUgyfel);
            this.UgyfelRepository.ChangeUserData(regiUgyfel);
            this.OnPropertyChanged();
        }

        /// <summary>
        /// Új számla nyitás
        /// </summary>
        public void UjSzamla()
        {
            this.AktivSzamla.TAKAREKKARTYA = false;
            this.Szamlak.Add(this.AktivSzamla);
            this.SzamlaRepository.Insert(this.AktivSzamla);
        }

        /// <summary>
        /// Számla módosítás
        /// </summary>
        /// <param name="regiszamla">Számla referencia</param>
        public void SzamlaModositas(SZAMLA regiszamla)
        {
            this.SzamlaMasolas(this.AktivSzamla, regiszamla);
            this.SzamlaRepository.ChangeSzamlaData(regiszamla);
        }
    }
}
