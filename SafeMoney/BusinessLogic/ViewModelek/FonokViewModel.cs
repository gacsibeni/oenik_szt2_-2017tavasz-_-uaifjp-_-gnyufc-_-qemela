﻿// <copyright file="FonokViewModel.cs" company="BBK Software">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BusinessLogic
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DataAccessLayer;
    using DataAccessLayer.Repositories;

    /// <summary>
    /// FonokViewModel
    /// </summary>
    public class FonokViewModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FonokViewModel"/> class.
        /// </summary>
        public FonokViewModel()
        {
            this.Entities = new DatabaseEntities();

            this.FonokRepository = new FonokRepository(this.Entities);

            this.UgyintezoRepository = new UgyintezoRepository(this.Entities);

            this.SzamlaRepository = new SzamlaRepository(this.Entities);

            this.TranzakcioRepository = new TranzakcioRepository(this.Entities);

            this.Ugyintezok = new ObservableCollection<UGYINTEZO>();

            this.Tranzakciok = new ObservableCollection<TRANZAKCIO>();

            this.Szamlak = new ObservableCollection<SZAMLA>();

            var q = from akt in this.UgyintezoRepository.GetAll()
                    select akt;

            foreach (var x in q)
            {
                this.Ugyintezok.Add(x);
            }

            var tr = from akt in this.TranzakcioRepository.GetAll()
                     select akt;
            foreach (var x in tr)
            {
                this.Tranzakciok.Add(x);
            }

            var szml = from x in this.SzamlaRepository.GetAll()
                       select x;
            foreach (var x in szml)
            {
                this.Szamlak.Add(x);
            }
        }

        /// <summary>
        /// Gets or sets entitás
        /// </summary>
        public DatabaseEntities Entities { get; set; }

        /// <summary>
        /// Gets or sets Fonokrepository
        /// </summary>
        public FonokRepository FonokRepository { get; set; }

        /// <summary>
        /// Gets or sets UgyintezoRepository
        /// </summary>
        public UgyintezoRepository UgyintezoRepository { get; set; }

        /// <summary>
        /// Gets or sets SzamlaRepository
        /// </summary>
        public SzamlaRepository SzamlaRepository { get; set; }

        /// <summary>
        /// Gets or sets TranzakcioRepository
        /// </summary>
        public TranzakcioRepository TranzakcioRepository { get; set; }

        /// <summary>
        /// Gets or sets Ugyintezok
        /// </summary>
        public ObservableCollection<UGYINTEZO> Ugyintezok { get; set; }

        /// <summary>
        /// Gets or sets Tranzakciok
        /// </summary>
        public ObservableCollection<TRANZAKCIO> Tranzakciok { get; set; }

        /// <summary>
        /// Gets or sets Szamlak
        /// </summary>
        public ObservableCollection<SZAMLA> Szamlak { get; set; }
    }
}
