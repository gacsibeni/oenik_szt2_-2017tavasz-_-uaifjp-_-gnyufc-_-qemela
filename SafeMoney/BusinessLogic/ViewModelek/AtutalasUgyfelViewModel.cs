﻿// <copyright file="AtutalasUgyfelViewModel.cs" company="BBK Software">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BusinessLogic
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Data;
    using DataAccessLayer;
    using DataAccessLayer.Repositories;

    /// <summary>
    /// Az átutalás ablak viewmodel osztálya.
    /// </summary>
    public class AtutalasUgyfelViewModel : INotifyPropertyChanged
    {
        private ICollectionView phrasesView;

        private string filter;

        /// <summary>
        /// Initializes a new instance of the <see cref="AtutalasUgyfelViewModel"/> class.
        /// Létrehoz egy új <see cref="AtutalasUgyfelViewModel"/> példányt.
        /// </summary>
        /// <param name="ugyfel">Bemeneti ügyfél.</param>
        /// <param name="entities">Bemeneti entities db példány.</param>
        public AtutalasUgyfelViewModel(UGYFEL ugyfel, DatabaseEntities entities)
        {
            this.Entities = entities;

            this.TranzakcioRepository = new TranzakcioRepository(this.Entities);

            this.SzamlaRepository = new SzamlaRepository(this.Entities);

            this.UgyfelRepository = new UgyfelRepository(this.Entities);

            this.Szamlak = new ObservableCollection<SZAMLA>();

            this.Ugyfel = ugyfel;

            var q = this.SzamlaRepository.GetAll();

            foreach (var x in q)
            {
                this.Szamlak.Add(x);
            }

            this.phrasesView = CollectionViewSource.GetDefaultView(this.Szamlak);
            this.phrasesView.Filter = o => string.IsNullOrEmpty(this.Filter) ? true : o.ToString().Contains(this.Filter);
        }

        /// <summary>
        /// PropertyChanged eseménykezelő.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets or sets Entities, that is the current entity of the database. Only one entity exists  at any time while in the client functions. That entity gets passed between viewmodels.
        /// Beállítja az Entities tulajdonság értékét, vagy lekéri az értékét.
        /// </summary>
        public DatabaseEntities Entities { get; set; }

        /// <summary>
        /// Gets or sets SzamlaRepository, for the window to be able to maintain the data of all Szamla entries in the database.
        /// Beállítja az SzamlaRepository tulajdonság értékét, vagy lekéri az értékét.
        /// </summary>
        public SzamlaRepository SzamlaRepository { get; set; }

        /// <summary>
        /// Gets or sets a UgyfelRepository property, for the window to be able to see clients.
        /// Beállítja az UgyfelRepository tulajdonság értékét.
        /// </summary>
        public UgyfelRepository UgyfelRepository { get; set; }

        /// <summary>
        /// Gets or sets a TranzakcioRepository, used when writing data into the database after changes affecting the database (e.g. money movement).
        /// Beállítja a TranzakcioRepository tulajdonság értékét, vagy lekéri az értékét.
        /// </summary>
        public TranzakcioRepository TranzakcioRepository { get; set; }

        /// <summary>
        /// Gets or sets Ugyfel property, which identifies the current client using the database and limits their access based on it.
        /// Beállítja az Ugyfel tulajdonság értékét, vagy lekéri az értékét.
        /// </summary>
        public UGYFEL Ugyfel { get; set; }

        /// <summary>
        /// Gets or sets Szamlak OC, for databrowsing in the window.
        /// Beállítja az Szamlak OC tulajdonság értékét, vagy lekéri az értékét.
        /// </summary>
        public ObservableCollection<SZAMLA> Szamlak { get; set; }

        /// <summary>
        /// Gets or sets Kivalasztott SZAMLA property for access through the window (e.g. client wants to send money to this Szamla).
        /// Beállítja az ablakban kijelölt számla értékét, vagy lekéri az értékét.
        /// </summary>
        public SZAMLA Kivalasztott { get; set; }

        /// <summary>
        /// Gets or sets the value of Filter property, which is a helper property of the search logic.
        /// Beállítja az filter segédlogika értékét, vagy lekéri az értékét, mely a keresőlogikához szükséges.
        /// </summary>
        public string Filter
        {
            get
            {
                return this.filter;
            }

            set
            {
                if (value != this.filter)
                {
                    this.filter = value;
                    this.phrasesView.Refresh();
                    this.OnPropChanged("Filter");
                }
            }
        }

        /// <summary>
        /// PropertyChanged esemény hívásakor futó metódus.
        /// </summary>
        /// <param name="s">A hívó tulajdonság neve.</param>
        protected void OnPropChanged([CallerMemberName] string s = "")
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(s));
        }
    }
}