﻿// <copyright file="LekotesViewModel.cs" company="BBK Software">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BusinessLogic.ViewModelek
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DataAccessLayer;
    using DataAccessLayer.Repositories;

    /// <summary>
    /// Lekötés ablak VM osztálya.
    /// </summary>
    public class LekotesViewModel : Bindable
    {
        private bool vanLekotese;

        private LEKOTES lekotes;

        /// <summary>
        /// Initializes a new instance of the <see cref="LekotesViewModel"/> class.
        /// Létrehoz egy <see cref="LekotesViewModel"/> példányt.
        /// </summary>
        /// <param name="ugyfel">Ugyfel paraméter az ablakhoz. Melyik ügyfél nyitotta meg.</param>
        /// <param name="entity">A kapott dbentity példány amelyen dolgozik az ablak.</param>
        public LekotesViewModel(UGYFEL ugyfel, DatabaseEntities entity)
        {
            this.Entities = entity;
            this.LekotesRepository = new LekotesRepository(entity);
            this.Ugyfel = ugyfel;
            try
            {
                this.Lekotes = this.LekotesRepository.GetAll().Single(l => l.SZAMLASZAM == this.Ugyfel.SZAMLASZAM);
            }
            catch (InvalidOperationException)
            {
                this.Lekotes = null;
            }

            if (this.Lekotes != null)
            {
                this.VanLekotese = true;
            }
            else
            {
                this.VanLekotese = false;
            }
        }

        /// <summary>
        /// Gets or sets the value of the database entities modified through this window.
        /// Beállítja a Entities tulajdonságot, vagy lekéri az értékét.
        /// </summary>
        public DatabaseEntities Entities { get; set; }

        /// <summary>
        /// Gets or sets the LekotesRepository, modified or browsed by the window.
        /// Beállítja a LekotesRepository tulajdonságot, vagy lekéri az értékét.
        /// </summary>
        public LekotesRepository LekotesRepository { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether current client has or does not have a bank deposit.
        /// Beállítja a VanLekotese tulajdonságot, vagy lekéri az értékét.
        /// </summary>
        public bool VanLekotese
        {
            get
            {
                return this.vanLekotese;
            }

            set
            {
                this.vanLekotese = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the value of Ugyfel, the client that browses the window.
        /// Beállítja az Ugyfel tulajdonságot, vagy lekéri az értékét.
        /// </summary>
        public UGYFEL Ugyfel { get; set; }

        /// <summary>
        /// Gets or sets the value of Lekotes, the deposit the current client has.
        /// Beállítja a Lekotes tulajdonságot, vagy lekéri az értékét.
        /// </summary>
        public LEKOTES Lekotes
        {
            get
            {
                return this.lekotes;
            }

            set
            {
                this.lekotes = value;
                this.OnPropertyChanged();
            }
        }
    }
}
