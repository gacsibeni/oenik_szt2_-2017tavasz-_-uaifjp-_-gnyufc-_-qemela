﻿// <copyright file="Bindable.cs" company="BBK Software">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DataAccessLayer
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Objektumok váltorásának eseménykezelője.
    /// </summary>
    public class Bindable : INotifyPropertyChanged
    {
        /// <summary>
        /// A mezők változásainak eseménykezelője.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Attribútum változás esetén ez fut le.
        /// </summary>
        /// <param name="propertyName">Ha nem adunk át semmit, akkor üres string lesz.</param>
        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
