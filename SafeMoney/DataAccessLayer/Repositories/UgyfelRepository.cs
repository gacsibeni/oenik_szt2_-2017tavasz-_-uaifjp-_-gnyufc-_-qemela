﻿// <copyright file="UgyfelRepository.cs" company="BBK Software">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DataAccessLayer.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DataAccessLayer.Interfaces;

    /// <summary>
    /// Ügyfeleket kezelő osztály
    /// </summary>
    public class UgyfelRepository : Repository<UGYFEL>, IUgyfelRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UgyfelRepository"/> class.
        /// </summary>
        /// <param name="entities">Ügyfél entitások.</param>
        public UgyfelRepository(DbContext entities)
            : base(entities)
        {
        }

        /// <summary>
        /// Név megváltoztatása.
        /// </summary>
        /// <param name="id">Ügyfél azonosítója.</param>
        /// <param name="newname">Ügyfél új neve</param>
        public void ChangeName(int id, string newname)
        {
            this.Entities.
                Set<UGYFEL>().
                Single(x => x.ID == id).
                NEV = newname;
            this.Entities.SaveChanges();
        }

        /// <summary>
        /// Ügyfél jelszavát változtása.
        /// </summary>
        /// <param name="id">Ügyfél azonosítója.</param>
        /// <param name="newpassword">Ügyfél új jelszava.</param>
        public void ChangePassword(int id, string newpassword)
        {
            this.Entities.
                Set<UGYFEL>().
                Single(x => x.ID == id).
                PW = newpassword;
            this.Entities.SaveChanges();
        }

        /// <summary>
        /// Ügyfél felhasznáálónevének megváltoztatása.
        /// </summary>
        /// <param name="id">Ügyfél azonosítója.</param>
        /// <param name="newusername">Ügyfél új felhasználóneve.</param>
        public void ChangeUsername(int id, string newusername)
        {
            this.Entities.
                Set<UGYFEL>().
                Single(x => x.ID == id).
                USERNAME = newusername;
            this.Entities.SaveChanges();
        }

        /// <summary>
        /// Ügyfél számlájának módosítása.
        /// </summary>
        /// <param name="id">Ügyfél azonosító.</param>
        /// <param name="nujSzamla">Új számla.</param>
        public void ChangeSzamla(int id, SZAMLA nujSzamla)
        {
            this.Entities.
                Set<UGYFEL>().
                Single(x => x.ID == id).
                SZAMLA = nujSzamla;
            this.Entities.SaveChanges();
        }

        /// <summary>
        /// Ügyfél valamennyi adatát módosítja.
        /// </summary>
        /// <param name="modositando">Ügyfél objektum már az új adatokkal.</param>
        public void ChangeUserData(UGYFEL modositando)
        {
            int id = (int)modositando.ID;
            this.ChangeName(id, modositando.NEV);
            this.ChangePassword(id, modositando.PW);
            this.ChangeUsername(id, modositando.USERNAME);
            this.ChangeSzamla(id, modositando.SZAMLA);
        }
    }
}
