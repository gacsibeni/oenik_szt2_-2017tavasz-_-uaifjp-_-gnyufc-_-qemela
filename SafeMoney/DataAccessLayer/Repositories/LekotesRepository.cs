﻿// <copyright file="LekotesRepository.cs" company="BBK Software">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DataAccessLayer.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DataAccessLayer.Interfaces;

    /// <summary>
    /// Lekötéseket kezelő osztály.
    /// </summary>
    public class LekotesRepository : Repository<LEKOTES>, ILekotesRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LekotesRepository"/> class.
        /// Létrehozza az objektumot.
        /// </summary>
        /// <param name="entities">Átadja a lekötéseket.</param>
        public LekotesRepository(DbContext entities)
            : base(entities)
        {
        }

        /// <summary>
        /// A lejárati dátum megváltoztatása.
        /// </summary>
        /// <param name="lekotesszam">Lekötés azonosítója.</param>
        /// <param name="newdate">Új dátum.</param>
        public void ChangeEndDate(decimal lekotesszam, DateTime newdate)
        {
            this.Entities.Set<LEKOTES>().Single(x => x.LEKOTESSZAM == lekotesszam).ENDDATE = newdate;
            this.Entities.SaveChanges();
        }

        /// <summary>
        /// A lekötött összeget változtatja meg.
        /// </summary>
        /// <param name="lekotesszam">Lekötés azonosítója.</param>
        /// <param name="plusorminus">true ha hozzáadás, false ha csökkentés.</param>
        /// <param name="value">Csökkentés/növelés mértéke.</param>
        public void ChangeValue(decimal lekotesszam, bool plusorminus, decimal value)
        {
            if (plusorminus)
            {
                this.Entities.Set<LEKOTES>().Single(x => x.SZAMLASZAM == lekotesszam).VISSZA += value;
            }
            else
            {
                this.Entities.Set<LEKOTES>().Single(x => x.SZAMLASZAM == lekotesszam).VISSZA -= value;
            }

            this.Entities.SaveChanges();
        }
    }
}
