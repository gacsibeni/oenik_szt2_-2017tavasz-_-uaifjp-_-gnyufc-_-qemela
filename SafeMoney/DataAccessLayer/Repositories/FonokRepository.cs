﻿// <copyright file="FonokRepository.cs" company="BBK Software">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace DataAccessLayer.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DataAccessLayer.Interfaces;

    /// <summary>
    /// Főnököket tartalmazó repository.
    /// </summary>
    public class FonokRepository : Repository<FONOK>, IFonokRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FonokRepository"/> class.
        /// Konstruktor, mely "elmenti" a kapott főnököket.
        /// </summary>
        /// <param name="entities">Főnökök átadása.</param>
        public FonokRepository(DbContext entities)
            : base(entities)
        {
        }

        /// <summary>
        /// Név megváltoztatása.
        /// </summary>
        /// <param name="id">Főnök azonosítója.</param>
        /// <param name="newname">Főnök új neve</param>
        public void ChangeName(int id, string newname)
        {
            this.Entities.
                Set<FONOK>().
                Single(x => x.ID == id).
                NEV = newname;
            this.Entities.SaveChanges();
        }

        /// <summary>
        /// Főnök jelszavát változtása.
        /// </summary>
        /// <param name="id">Főnök azonosítója.</param>
        /// <param name="newpassword">Főnök új jelszava.</param>
        public void ChangePassword(int id, string newpassword)
        {
            this.Entities.
                Set<FONOK>().
                Single(x => x.ID == id).
                PW = newpassword;
            this.Entities.SaveChanges();
        }

        /// <summary>
        /// Főnök felhasznáálónevének megváltoztatása.
        /// </summary>
        /// <param name="id">Főnök azonosítója.</param>
        /// <param name="newusername">FőnökS új felhasználóneve.</param>
        public void ChangeUsername(int id, string newusername)
        {
            this.Entities.
                Set<FONOK>().
                Single(x => x.ID == id).
                USERNAME = newusername;
            this.Entities.SaveChanges();
        }
    }
}
