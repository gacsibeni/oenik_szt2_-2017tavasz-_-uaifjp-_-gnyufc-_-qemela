﻿// <copyright file="UgyintezoRepository.cs" company="BBK Software">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DataAccessLayer.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DataAccessLayer.Interfaces;

    /// <summary>
    /// Ügyintézőket kezelő osztály.
    /// </summary>
    public class UgyintezoRepository : Repository<UGYINTEZO>, IUgyinzetoRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UgyintezoRepository"/> class.
        /// </summary>
        /// <param name="entities">Ügyintééző entitások.</param>
        public UgyintezoRepository(DbContext entities)
            : base(entities)
        {
        }

        /// <summary>
        /// Ügyintézők nevének módosítása.
        /// </summary>
        /// <param name="id">Ügyfél azonosító.</param>
        /// <param name="newname">Ügyfél új neve.</param>
        public void ChangeName(int id, string newname)
        {
            this.Entities.
                Set<UGYINTEZO>().
                Single(x => x.ID == id).
                NEV = newname;
            this.Entities.SaveChanges();
        }

        /// <summary>
        /// Ügyfél jelszavának megváltoztatása.
        /// </summary>
        /// <param name="id">Ügyfél azonosítója.</param>
        /// <param name="newpassword">Ügyfél új jelszava.</param>
        public void ChangePassword(int id, string newpassword)
        {
            this.Entities.
                Set<UGYINTEZO>().
                Single(x => x.ID == id).
                PW = newpassword;
            this.Entities.SaveChanges();
        }

        /// <summary>
        /// Ügyfél felhasználónevének módosítása.
        /// </summary>
        /// <param name="id">Ügyfél azonosítója.</param>
        /// <param name="newusername">Ügyfél új felhasználóneve.</param>
        public void ChangeUsername(int id, string newusername)
        {
            this.Entities.
                Set<UGYINTEZO>().
                Single(x => x.ID == id).
                USERNAME = newusername;
            this.Entities.SaveChanges();
        }
    }
}
