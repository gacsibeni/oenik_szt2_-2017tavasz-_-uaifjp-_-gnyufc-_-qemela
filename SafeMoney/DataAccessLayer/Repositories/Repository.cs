﻿// <copyright file="Repository.cs" company="BBK Software">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DataAccessLayer.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// A különböző repositoryk ebből a classból öröklődnek.
    /// </summary>
    /// <typeparam name="TEntity">Entitást adunk át</typeparam>
    public class Repository<TEntity> : Interfaces.IRepository<TEntity>
        where TEntity : class
    {
        /// <summary>
        /// A repositoryk használják ezt különböző lekérdezésekben.
        /// </summary>
        private DbContext entities;

        /// <summary>
        /// Initializes a new instance of the <see cref="Repository{TEntity}"/> class.
        /// Objektum létrehozása, és alap attribútumok beállítása
        /// </summary>
        /// <param name="entities">Egy DBContext entitás.</param>
        public Repository(DbContext entities)
        {
            this.Entities = entities;
        }

        /// <summary>
        /// Gets or sets entities.
        /// </summary>
        protected DbContext Entities { get => this.entities; set => this.entities = value; }

        /// <summary>
        /// Egy entitás törlése
        /// </summary>
        /// <param name="entitytodelete">Törlendő entitás.</param>
        public void Delete(TEntity entitytodelete)
        {
            this.Entities.Set<TEntity>().Remove(entitytodelete);
            this.Entities.SaveChanges();
        }

        /// <summary>
        /// Visszaadja az összes elemet.
        /// </summary>
        /// <returns>Összes elem.</returns>
        public IQueryable<TEntity> GetAll()
        {
            return this.Entities.Set<TEntity>();
        }

        /// <summary>
        /// Elem beszúrása.
        /// </summary>
        /// <param name="entitytoadd">Beszúrandó elem.</param>
        public void Insert(TEntity entitytoadd)
        {
                this.Entities.Set<TEntity>().Add(entitytoadd);
                this.Entities.SaveChanges();
        }
    }
}
