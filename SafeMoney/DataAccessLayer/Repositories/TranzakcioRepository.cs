﻿// <copyright file="TranzakcioRepository.cs" company="BBK Software">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DataAccessLayer.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DataAccessLayer.Interfaces;

    /// <summary>
    /// Tranzakciókat kezeő osztály.
    /// </summary>
    public class TranzakcioRepository : Repository<TRANZAKCIO>, ITranzakcioRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TranzakcioRepository"/> class.
        /// Objektum létrehozása.
        /// </summary>
        /// <param name="entities">Tranzakciók.</param>
        public TranzakcioRepository(DbContext entities)
            : base(entities)
        {
        }
    }
}
