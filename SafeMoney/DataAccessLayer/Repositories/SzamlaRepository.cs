﻿// <copyright file="SzamlaRepository.cs" company="BBK Software">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DataAccessLayer.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DataAccessLayer.Interfaces;

    /// <summary>
    /// Számlákat kezelő osztály
    /// </summary>
    public class SzamlaRepository : Repository<SZAMLA>, ISzamlaRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SzamlaRepository"/> class.
        /// Létrehozza az objektumot.
        /// </summary>
        /// <param name="entities">Számlák átadása.</param>
        public SzamlaRepository(DbContext entities)
            : base(entities)
        {
        }

        /// <summary>
        /// A számla létrehozásának dátumát változtatja.
        /// </summary>
        /// <param name="szamlaszam">Számla azonosítója.</param>
        /// <param name="newdate">Új dátum.</param>
        public void ChangeDate(decimal szamlaszam, DateTime newdate)
        {
            this.Entities.Set<SZAMLA>().Single(x => x.SZAMLASZAM == szamlaszam).STARTDATE = newdate;
            this.Entities.SaveChanges();
        }

        /// <summary>
        /// Számlán lévő összeg módosítása.
        /// </summary>
        /// <param name="szamlaszam">Számla azonosítója.</param>
        /// <param name="plusorminus">true ha hozzáadás, false ha csökkentés</param>
        /// <param name="amount">Csökkentés/növelés mértéke.</param>
        public void ChangeFunds(decimal szamlaszam, bool plusorminus, int amount)
        {
            if (plusorminus)
            {
                this.Entities.Set<SZAMLA>().Single(x => x.SZAMLASZAM == szamlaszam).EGYENLEG += amount;
            }
            else
            {
                this.Entities.Set<SZAMLA>().Single(x => x.SZAMLASZAM == szamlaszam).EGYENLEG -= amount;
            }

            this.Entities.SaveChanges();
        }

        /// <summary>
        /// Tulajdonos megváltoztatása.
        /// </summary>
        /// <param name="szamlaszam">Számla azonosítója.</param>
        /// <param name="newowner">Új tulajdonos.</param>
        public void ChangeOwner(decimal szamlaszam, string newowner)
        {
            this.Entities.Set<SZAMLA>().Single(x => x.SZAMLASZAM == szamlaszam).TULAJDONOS = newowner;
            this.Entities.SaveChanges();
        }

        /// <summary>
        /// Takarékkártya állapotának átbillentése.
        /// </summary>
        /// <param name="szamlaszam">Számla azonosítója.</param>
        public void ChangeTakarekKartya(decimal szamlaszam)
        {
            var objecttomodify = this.Entities.Set<SZAMLA>().Single(x => x.SZAMLASZAM == szamlaszam);
            if (objecttomodify.TAKAREKKARTYA == true)
            {
                objecttomodify.TAKAREKKARTYA = false;
            }
            else
            {
                objecttomodify.TAKAREKKARTYA = true;
            }

            this.Entities.SaveChanges();
        }

        /// <summary>
        /// Számla összes változtatható adatát megváltoztatja.
        /// </summary>
        /// <param name="ujSzamla">Módosított számla.</param>
        public void ChangeSzamlaData(SZAMLA ujSzamla)
        {
            decimal szamlaszam = ujSzamla.SZAMLASZAM;
            this.ChangeDate(szamlaszam, (DateTime)ujSzamla.STARTDATE);
            this.ChangeOwner(szamlaszam, ujSzamla.TULAJDONOS);
        }
    }
}
