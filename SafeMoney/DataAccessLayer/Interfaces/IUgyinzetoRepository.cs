﻿// <copyright file="IUgyinzetoRepository.cs" company="BBK Software">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DataAccessLayer.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Ügyintézőkezelő osztály interfésze.
    /// </summary>
    public interface IUgyinzetoRepository : IRepository<UGYINTEZO>
    {
        /// <summary>
        /// Ügyintézők nevének módosítása.
        /// </summary>
        /// <param name="id">Ügyfél azonosító.</param>
        /// <param name="newname">Ügyfél új neve.</param>
        void ChangeName(int id, string newname);

        /// <summary>
        /// Ügyfél jelszavának megváltoztatása.
        /// </summary>
        /// <param name="id">Ügyfél azonosítója.</param>
        /// <param name="newpassword">Ügyfél új jelszava.</param>
        void ChangePassword(int id, string newpassword);

        /// <summary>
        /// Ügyfél felhasználónevének módosítása.
        /// </summary>
        /// <param name="id">Ügyfél azonosítója.</param>
        /// <param name="newusername">Ügyfél új felhasználóneve.</param>
        void ChangeUsername(int id, string newusername);
    }
}
