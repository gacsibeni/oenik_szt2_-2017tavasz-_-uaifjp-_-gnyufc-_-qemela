﻿// <copyright file="IFonokRepository.cs" company="BBK Software">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DataAccessLayer.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Főnököket kezelő interfész.
    /// </summary>
    public interface IFonokRepository : IRepository<FONOK>
    {
        /// <summary>
        /// Főnökök nevének változtatása.
        /// </summary>
        /// <param name="id">Főnök azonosítója.</param>
        /// <param name="newname">Főnök új neve.</param>
        void ChangeName(int id, string newname);

        /// <summary>
        /// Főnök jelszavát változtása.
        /// </summary>
        /// <param name="id">Főnök azonosítója.</param>
        /// <param name="newpassword">Főnök új jelszava.</param>
        void ChangePassword(int id, string newpassword);

        /// <summary>
        /// Főnök felhasznáálónevének megváltoztatása.
        /// </summary>
        /// <param name="id">Főnök azonosítója.</param>
        /// <param name="newusername">Főnök új felhasználóneve.</param>
        void ChangeUsername(int id, string newusername);
    }
}
