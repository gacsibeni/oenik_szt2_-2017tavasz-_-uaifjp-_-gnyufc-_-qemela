﻿// <copyright file="IUgyfelRepository.cs" company="BBK Software">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DataAccessLayer.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Ügyfeleket tartalmazó repository iterfésze
    /// </summary>
    public interface IUgyfelRepository : IRepository<UGYFEL>
    {
        /// <summary>
        /// Név megváltoztatása.
        /// </summary>
        /// <param name="id">Ügyfél azonosítója.</param>
        /// <param name="newname">Ügyfél új neve</param>
        void ChangeName(int id, string newname);

        /// <summary>
        /// Ügyfél jelszavát változtása.
        /// </summary>
        /// <param name="id">Ügyfél azonosítója.</param>
        /// <param name="newpassword">Ügyfél új jelszava.</param>
        void ChangePassword(int id, string newpassword);

        /// <summary>
        /// Ügyfél felhasznáálónevének megváltoztatása.
        /// </summary>
        /// <param name="id">Ügyfél azonosítója.</param>
        /// <param name="newusername">Ügyfél új felhasználóneve.</param>
        void ChangeUsername(int id, string newusername);
    }
}
