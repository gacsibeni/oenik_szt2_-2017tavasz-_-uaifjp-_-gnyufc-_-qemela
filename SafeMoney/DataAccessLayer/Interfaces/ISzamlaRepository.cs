﻿// <copyright file="ISzamlaRepository.cs" company="BBK Software">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DataAccessLayer.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Számlakezelő osztály metódusait előíró interfész.
    /// </summary>
    public interface ISzamlaRepository : IRepository<SZAMLA>
    {
        /// <summary>
        /// A számla létrehozásának dátumát változtatja.
        /// </summary>
        /// <param name="szamlaszam">Számla azonosítója.</param>
        /// <param name="newdate">Új dátum.</param>
        void ChangeDate(decimal szamlaszam, DateTime newdate);

        /// <summary>
        /// Tulajdonos megváltoztatása.
        /// </summary>
        /// <param name="szamlaszam">Számla azonosítója.</param>
        /// <param name="newowner">Új tulajdonos.</param>
        void ChangeOwner(decimal szamlaszam, string newowner);

        /// <summary>
        /// Takarékkártya állapotának átbillentése.
        /// </summary>
        /// <param name="szamlaszam">Számla azonosítója.</param>
        void ChangeTakarekKartya(decimal szamlaszam);

        /// <summary>
        /// Számlán lévő összeg módosítása.
        /// </summary>
        /// <param name="szamlaszam">Számla azonosítója.</param>
        /// <param name="plusorminus">true ha hozzáadás, false ha csökkentés</param>
        /// <param name="amount">Csökkentés/növelés mértéke.</param>
        void ChangeFunds(decimal szamlaszam, bool plusorminus, int amount);
    }
}
