﻿// <copyright file="ITranzakcioRepository.cs" company="BBK Software">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DataAccessLayer.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Tranzakciókezelő osztály metódusait írja elő.
    /// </summary>
    public interface ITranzakcioRepository : IRepository<TRANZAKCIO>
    {
        // Tranzakciók adatait nem változtatgatjuk, csak magát a tranzakciót töröljük, ezek BL-ből megoldva!
    }
}
