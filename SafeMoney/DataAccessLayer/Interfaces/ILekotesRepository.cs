﻿// <copyright file="ILekotesRepository.cs" company="BBK Software">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DataAccessLayer.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Lekötéseket kezelő osztály metódusait előíró interfész.
    /// </summary>
    public interface ILekotesRepository : IRepository<LEKOTES>
    {
        /// <summary>
        /// Lejárati dátum módosítása.
        /// </summary>
        /// <param name="lekotesszam">Lekötés azonosító</param>
        /// <param name="newdate">Új dátum.</param>
        void ChangeEndDate(decimal lekotesszam, DateTime newdate);

        /// <summary>
        /// A lekötött összeget változtatja meg.
        /// </summary>
        /// <param name="lekotesszam">Lekötés azonosítója.</param>
        /// <param name="plusorminus">true ha hozzáadás, false ha csökkentés.</param>
        /// <param name="value">Csökkentés/növelés mértéke.</param>
        void ChangeValue(decimal lekotesszam, bool plusorminus, decimal value);
    }
}
