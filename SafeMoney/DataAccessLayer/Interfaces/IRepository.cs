﻿// <copyright file="IRepository.cs" company="BBK Software">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DataAccessLayer.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Repository osztály interfésze.
    /// </summary>
    /// <typeparam name="TEntity">Entitás típus.</typeparam>
    public interface IRepository<TEntity>
        where TEntity : class
    {
        /// <summary>
        /// Új entitás beillesztése.
        /// </summary>
        /// <param name="entitytoadd">Beillesztendő entitás.</param>
        void Insert(TEntity entitytoadd);

        /// <summary>
        /// Entitás törlése.
        /// </summary>
        /// <param name="entitytodelete">Törlendő entitás.</param>
        void Delete(TEntity entitytodelete);

        /// <summary>
        /// Visszaadja az összes entitást.
        /// </summary>
        /// <returns>Entitások.</returns>
        IQueryable<TEntity> GetAll();
    }
}
