var searchData=
[
  ['dataaccesslayer',['DataAccessLayer',['../namespace_data_access_layer.html',1,'']]],
  ['databaseentities',['DatabaseEntities',['../class_data_access_layer_1_1_database_entities.html',1,'DataAccessLayer.DatabaseEntities'],['../class_data_access_layer_1_1_database_entities.html#af10e5fe4de1ff2efbd7c04610431d5a2',1,'DataAccessLayer.DatabaseEntities.DatabaseEntities()']]],
  ['datum',['DATUM',['../class_data_access_layer_1_1_t_r_a_n_z_a_k_c_i_o.html#aaa067907ef94340a4f9962d43fe2d37c',1,'DataAccessLayer::TRANZAKCIO']]],
  ['datumokfeltolt',['DatumokFeltolt',['../class_business_logic_1_1_fonok_lekerdezesek_1_1_diagram_metodusok.html#a6a96be2f01ea027c7787baea03d192e6',1,'BusinessLogic::FonokLekerdezesek::DiagramMetodusok']]],
  ['datumparosit',['DatumParosit',['../class_business_logic_1_1_fonok_lekerdezesek_1_1_diagram_metodusok.html#a5bcf3f210f86b1acade3f68cd41e5bd4',1,'BusinessLogic::FonokLekerdezesek::DiagramMetodusok']]],
  ['delete',['Delete',['../interface_data_access_layer_1_1_interfaces_1_1_i_repository.html#a9b25de37dc5b2828a05a9d0d61c136dc',1,'DataAccessLayer.Interfaces.IRepository.Delete()'],['../class_data_access_layer_1_1_repositories_1_1_repository.html#a95793058ca2a76a37afb4d7b3ae66ce4',1,'DataAccessLayer.Repositories.Repository.Delete()']]],
  ['diagram',['Diagram',['../class_safe_money_1_1_fonok_1_1_diagram.html',1,'SafeMoney.Fonok.Diagram'],['../class_safe_money_1_1_fonok_1_1_diagram.html#adc9c89904688aa84813df7723a883b13',1,'SafeMoney.Fonok.Diagram.Diagram()']]],
  ['diagramertekpar',['DiagramErtekPar',['../class_business_logic_1_1_fonok_lekerdezesek_1_1_diagram_ertek_par.html',1,'BusinessLogic::FonokLekerdezesek']]],
  ['diagrammetodusok',['DiagramMetodusok',['../class_business_logic_1_1_fonok_lekerdezesek_1_1_diagram_metodusok.html',1,'BusinessLogic.FonokLekerdezesek.DiagramMetodusok'],['../class_business_logic_1_1_fonok_lekerdezesek_1_1_diagram_metodusok.html#a0b236063fd520191e9599ac25796aa72',1,'BusinessLogic.FonokLekerdezesek.DiagramMetodusok.DiagramMetodusok()']]],
  ['interfaces',['Interfaces',['../namespace_data_access_layer_1_1_interfaces.html',1,'DataAccessLayer']]],
  ['repositories',['Repositories',['../namespace_data_access_layer_1_1_repositories.html',1,'DataAccessLayer']]]
];
