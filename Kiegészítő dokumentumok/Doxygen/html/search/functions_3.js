var searchData=
[
  ['databaseentities',['DatabaseEntities',['../class_data_access_layer_1_1_database_entities.html#af10e5fe4de1ff2efbd7c04610431d5a2',1,'DataAccessLayer::DatabaseEntities']]],
  ['datumokfeltolt',['DatumokFeltolt',['../class_business_logic_1_1_fonok_lekerdezesek_1_1_diagram_metodusok.html#a6a96be2f01ea027c7787baea03d192e6',1,'BusinessLogic::FonokLekerdezesek::DiagramMetodusok']]],
  ['datumparosit',['DatumParosit',['../class_business_logic_1_1_fonok_lekerdezesek_1_1_diagram_metodusok.html#a5bcf3f210f86b1acade3f68cd41e5bd4',1,'BusinessLogic::FonokLekerdezesek::DiagramMetodusok']]],
  ['delete',['Delete',['../interface_data_access_layer_1_1_interfaces_1_1_i_repository.html#a9b25de37dc5b2828a05a9d0d61c136dc',1,'DataAccessLayer.Interfaces.IRepository.Delete()'],['../class_data_access_layer_1_1_repositories_1_1_repository.html#a95793058ca2a76a37afb4d7b3ae66ce4',1,'DataAccessLayer.Repositories.Repository.Delete()']]],
  ['diagram',['Diagram',['../class_safe_money_1_1_fonok_1_1_diagram.html#adc9c89904688aa84813df7723a883b13',1,'SafeMoney::Fonok::Diagram']]],
  ['diagrammetodusok',['DiagramMetodusok',['../class_business_logic_1_1_fonok_lekerdezesek_1_1_diagram_metodusok.html#a0b236063fd520191e9599ac25796aa72',1,'BusinessLogic::FonokLekerdezesek::DiagramMetodusok']]]
];
