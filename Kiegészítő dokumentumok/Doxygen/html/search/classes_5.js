var searchData=
[
  ['ifonokrepository',['IFonokRepository',['../interface_data_access_layer_1_1_interfaces_1_1_i_fonok_repository.html',1,'DataAccessLayer::Interfaces']]],
  ['ilekotesrepository',['ILekotesRepository',['../interface_data_access_layer_1_1_interfaces_1_1_i_lekotes_repository.html',1,'DataAccessLayer::Interfaces']]],
  ['irepository',['IRepository',['../interface_data_access_layer_1_1_interfaces_1_1_i_repository.html',1,'DataAccessLayer::Interfaces']]],
  ['irepository_3c_20fonok_20_3e',['IRepository&lt; FONOK &gt;',['../interface_data_access_layer_1_1_interfaces_1_1_i_repository.html',1,'DataAccessLayer::Interfaces']]],
  ['irepository_3c_20lekotes_20_3e',['IRepository&lt; LEKOTES &gt;',['../interface_data_access_layer_1_1_interfaces_1_1_i_repository.html',1,'DataAccessLayer::Interfaces']]],
  ['irepository_3c_20szamla_20_3e',['IRepository&lt; SZAMLA &gt;',['../interface_data_access_layer_1_1_interfaces_1_1_i_repository.html',1,'DataAccessLayer::Interfaces']]],
  ['irepository_3c_20tranzakcio_20_3e',['IRepository&lt; TRANZAKCIO &gt;',['../interface_data_access_layer_1_1_interfaces_1_1_i_repository.html',1,'DataAccessLayer::Interfaces']]],
  ['irepository_3c_20ugyfel_20_3e',['IRepository&lt; UGYFEL &gt;',['../interface_data_access_layer_1_1_interfaces_1_1_i_repository.html',1,'DataAccessLayer::Interfaces']]],
  ['irepository_3c_20ugyintezo_20_3e',['IRepository&lt; UGYINTEZO &gt;',['../interface_data_access_layer_1_1_interfaces_1_1_i_repository.html',1,'DataAccessLayer::Interfaces']]],
  ['iszamlarepository',['ISzamlaRepository',['../interface_data_access_layer_1_1_interfaces_1_1_i_szamla_repository.html',1,'DataAccessLayer::Interfaces']]],
  ['itranzakciorepository',['ITranzakcioRepository',['../interface_data_access_layer_1_1_interfaces_1_1_i_tranzakcio_repository.html',1,'DataAccessLayer::Interfaces']]],
  ['iugyfelrepository',['IUgyfelRepository',['../interface_data_access_layer_1_1_interfaces_1_1_i_ugyfel_repository.html',1,'DataAccessLayer::Interfaces']]],
  ['iugyinzetorepository',['IUgyinzetoRepository',['../interface_data_access_layer_1_1_interfaces_1_1_i_ugyinzeto_repository.html',1,'DataAccessLayer::Interfaces']]]
];
