var indexSectionsWithContent =
{
  0: "abcdefgiklmnoprstuvx",
  1: "abdfgilmrstuv",
  2: "bdstx",
  3: "abcdfgiklmnorstu",
  4: "a",
  5: "bce",
  6: "abcdefiklnopstuv",
  7: "p",
  8: "c"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "enums",
  5: "enumvalues",
  6: "properties",
  7: "events",
  8: "pages"
};

var indexSectionLabels =
{
  0: "Összes",
  1: "Osztályok",
  2: "Névtér",
  3: "Függvények",
  4: "Enumerációk",
  5: "Enumeráció-értékek",
  6: "Tulajdonságok",
  7: "Események",
  8: "Oldal"
};

